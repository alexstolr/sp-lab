#include <stdio.h>

/* forward declarations: */
void rec_map(char*, int, char (*f) (char));

struct char_array {
  char *array;
  int sz;
  void (*map) (struct char_array *, char (*f) (char));
};

/*applies the function f to each of carray's elements, changing their value accordingly.*/
void char_array_map(struct char_array *carray, char (*f) (char)){
  rec_map(carray->array, carray->sz, f);
}

/*initializes an allocated struct char_array pointed by 'carray' with the given array and size
 *. This function must also initialize the field 'map' of the structure to a */
void initialize_char_array(struct char_array *carray, char *array, int sz){
  carray->array = array;
  carray->sz = sz;
  carray->map = &char_array_map;
}

/* Converts c to lower case and return it. Characters that do not have a lower case, are returned unchanged */
char to_lower(char c){
  if(c>=65 && c <=90){
    c=c+32;
  }
  return c;
}
/* Converts c to upper case and return it. Characters that do not have an upper case, are returned unchanged */            
char to_upper(char c){
  if(c>=97 && c <= 122){
    c=c-32;
  }
  return c;
}

/* Prints the value of c, followed by a new line, and returns c unchanged */
char cprt(char c){
  printf("%c\n",c);
  return c;
}

/*receives an array, its size and a function's pointer,
 * for each array element replaces its value with the result of applying the function f on it.*/
void rec_map(char *array, int sz, char (*f) (char)){
  if(sz >= 0){
    rec_map(array,sz - 1,f);
    array[sz] = f(array[sz]);
    return;
  }
  else{
    return;
  }
}

int main(int argc, char **argv){
  /*test for Task2a*/
  
  /*
   * printf(" Converts C to lower case: %c\n", to_lower('C'));
   * printf(" Converts c to lower case: %c\n", to_upper('c'));
   * char c1 = cprt('d'); 
   */
  
  /*test for Task2b*/
  
  /*char c[] = {'L','a','b','2'};
  rec_map(c,4,cprt);
  rec_map(c,4,to_lower);
  rec_map(c,4,cprt);
  rec_map(c,4,to_upper);
  rec_map(c,4,cprt);
  */
  
  /*char c2[] = {'L','a','b','2'};
  int n = sizeof(c2)/sizeof(c2[0]);
  rec_map(c2,n,cprt);
  rec_map(c2,n,to_lower);
  rec_map(c2,n,cprt);
  rec_map(c2,n,to_upper);
  rec_map(c2,n,cprt);
  */
  
  
  /*test for Task2c*/
  
  /*
   * char array[4]={'L','a','b','2'};
   * struct char_array carray;
   * initialize_char_array(&carray, array, 4);
   * carray.map(&carray, cprt);
   * carray.map(&carray, to_lower);
   * carray.map(&carray, cprt);
   */
  
  return 0;
  }