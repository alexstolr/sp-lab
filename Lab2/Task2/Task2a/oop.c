#include <stdio.h>
 
/* forward declarations: */
void rec_map(char*, int, char (*f) (char));
 
struct char_array {
  char *array;
  int sz;
  void (*map) (struct char_array *, char (*f) (char));
};
 
void char_array_map(struct char_array *carray, char (*f) (char)){
  /* TODO: complete during task 2.c */
}
 
void initialize_char_array(struct char_array *carray, char *array, int sz){
  /* TODO: complete during task 2.c */
}
 
/* Converts c to lower case and return it. Characters that do not have a lower case, are returned unchanged */
char to_lower(char c){
 if(c>=65 && c <=90){
   c=c+32;
 }
 return c;
}
/* Converts c to upper case and return it. Characters that do not have an upper case, are returned unchanged */            
char to_upper(char c){
  if(c>=97 && c <= 122){
    c=c-32;
  }
  return c;
}

/* Prints the value of c, followed by a new line, and returns c unchanged */
char cprt(char c){
  printf("%d\n",c);
  return c;
}
 
 
void rec_map(char *array, int sz, char (*f) (char)){
  /* TODO: Complete during task 2.b */
}
 
int main(int argc, char **argv){
  /*next 3 lines are for testing of function implemented in Task2a*/
  printf(" Converts C to lower case: %c\n", to_lower('C'));
  printf(" Converts c to lower case: %c\n", to_upper('c'));
  printf("%c\n",cprt('c'));
  return 0;
}