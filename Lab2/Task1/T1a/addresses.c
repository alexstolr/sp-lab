﻿#include <stdio.h>
#include <string.h>
#include <stdlib.h>

int addr5;/* bss - unnitialized Data Segment*/
int addr6;/* bss - unnitialized Data Segment*/

int foo();/* text - because you want to share it from different places and want to save up space*/
void point_at(void *p);/* text - because you want to share it from different places and want to save up space*/

int main (int argc, char** argv){
    int addr2;/* stack*/
    int addr3;/* stack*/
    char* yos="ree"; /*yos stack ree text */
    int * addr4 = (int*)(malloc(50));  /* addr4 - on stack. its allocatod and can write, so on heap*/
    printf("- &addr2: %p\n",&addr2);
    printf("- &addr3: %p\n",&addr3);
    printf("- foo: %p\n",foo);
    printf("- &addr5: %p\n",&addr5);
    
	point_at(&addr5);
	
    printf("- &addr6: %p\n",&addr6);
    printf("- yos: %p\n",yos);
    printf("- addr4: %p\n",addr4);
    printf("- &addr4: %p\n",&addr4);
    return 0;
}

int foo(){ /* text - because you want to share it from different places and want to save up space*/
    return -1;
}

void point_at(void *p){
    int local;/* stack*/
	static int addr0 = 2; /*Initialized Data Segment - static */
    static int addr1;            /* bss - unnitialized Data Segment*/


    long dist1 = (size_t)&addr6 - (size_t)p; /*heap or stack?*/
    long dist2 = (size_t)&local - (size_t)p;/*heap or stack?*/
    long dist3 = (size_t)&foo - (size_t)p;/*heap or stack?*/
    
    printf("dist1: (size_t)&addr6 - (size_t)p: %ld\n",dist1);
    printf("dist2: (size_t)&local - (size_t)p: %ld\n",dist2);
    printf("dist3: (size_t)&foo - (size_t)p:  %ld\n",dist3);
	
	printf("- addr0: %p\n", & addr0);
    printf("- addr1: %p\n",&addr1);
}

/*What can you say about the numerical values? Do they obey a particular order?
 yes - the higher the address it means its closer to the command lone arguments and stack.
 lower means is closer to text than to the initialized data than to the bss and the heap.*/
