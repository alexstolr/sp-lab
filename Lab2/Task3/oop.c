#include <stdio.h>
#include <stdlib.h>

/* forward declarations: */
void rec_map(char*, int, char (*f) (char));

struct char_array {
  char *array;
  int sz;
  void (*map) (struct char_array *, char (*f) (char));
};

struct fun_desc {
  char *name;
  char (*fun)(char);
};

/*applies the function f to each of carray's elements, changing their value accordingly.*/
void char_array_map(struct char_array *carray, char (*f) (char)){
  rec_map(carray->array, carray->sz, f);
}

/*initializes an allocated struct char_array pointed by 'carray' with the given array and size
 *. This function must also initialize the field 'map' of the structure to a */
void initialize_char_array(struct char_array *carray, char *array, int sz){
  carray->array = array;
  carray->sz = sz;
  carray->map = &char_array_map;
}

/* Converts char to lower case and returns it. Characters that do not have a lower case, are returned unchanged */
char to_lower(char c){
  if(c>=65 && c <=90){
    c=c+32;
  }
  return c;
}
/* Converts char to upper case and returns it. Characters that do not have an upper case, are returned unchanged */            
char to_upper(char c){
  if(c>=97 && c <= 122){
    c=c-32;
  }
  return c;
}

/* Prints the value of c, followed by a new line, and returns c unchanged */
char cprt(char c){
  printf("%c\n",c);
  return c;
}

/*receives an array, its size and a function's pointer,
 * for each array element replaces its value with the result of applying the function f on it.*/
void rec_map(char *array, int sz, char (*f) (char)){
  if(sz==0){
    return; 
  }else{
    rec_map(array,sz-1,f);
    *(array+sz-1)=(*f)(*(array+sz-1));
  }
  return;
} 
/*if(sz >= 0){
 *    rec_map(array,sz - 1,f);
 *    array[sz] = f(array[sz]);
 *    return;
 *  }
 *  else{
 *    return;
 * }
 * }*/


int main(int argc, char **argv){
  
  
  struct char_array carray;
  char array[10];
  int sz;
  char tmp[3];
  int i;
  
  printf("initializing 'carray'...\n\n\n");
  /*initialize size*/
  printf("%s\n","Please enter array size (0<size<=10): ");
  fgets(tmp,4,stdin);
  sz= atoi(tmp);
  if(sz > 10){
    printf("size should be <= 10\n");
    exit(1);
  }
  /*initialize array*/
  for(i=0;i<sz;i++){
    printf("Please enter array[%d]:\n",i);
    fgets(tmp,11,stdin);
    array[i]= tmp[0];
  }
  
  initialize_char_array(&carray,array,sz);
  /*Defines an array of fun_desc and initializes it
   *to the names and the pointers of the three functions implemented in Task 2a*/
  struct fun_desc menu[3] = {
    {"0) To Lower Case",&to_lower},
    {"1) To Upper Case",&to_upper},
    {"2) Print",&cprt}
  };
  
  fflush(stdin);
  int index;
  int flag = 0;
  for(index = 0;index < 3 && flag == 0; index++){
    printf("please choose a function:\n 0)To Lower Case\n 1)To Upper Case\n 2)Print\n Option:");
    fgets(tmp,10,stdin);
    int funcChosen=atoi(tmp);
    if(funcChosen < 0 || funcChosen>=3){
      printf("ERROR, incorrect input\n");
      exit(1);
    }
    if(funcChosen == 2)
      flag = 1;
    carray.map(&carray,menu[funcChosen].fun);
    printf("Done\n");
  }
  
  return 0;
}













