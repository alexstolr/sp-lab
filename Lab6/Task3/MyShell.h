

typedef struct Pair{
  char *  name; /* a variable name that appears in the command line - example: "i" */
  char *  value;	/* value of name. example: when command is "$i" output is "hello" */
  struct Pair * next;	/* next stringPair in chain */
} Pair;


void execute(cmdLine * line);
Pair * getLink(Pair * list , char * name);
Pair * assign(char * name,char * value, Pair * list);
Pair * append(Pair * list , Pair * link);
Pair * env(Pair * list);
void changeDir(cmdLine * line);
char * getValue(char * name, Pair * list);
void freeAll(char * commandHistory[],Pair * list,int * commandsCount);
void testPrint(int printIndex,char * string, cmdLine * line,int addressFlag);
void ioRedirect(cmdLine * line); 
