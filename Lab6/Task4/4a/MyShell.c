#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <errno.h>
#include <sys/wait.h>
#include <string.h>
#include <sys/types.h>  /* Primitive System Data Types */ 
#include <limits.h> /* PATH_MAX */
#include "LineParser.h"
#include "MyShell.h"
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>

#define MAX_LINE_SIZE 2048
#define MAX_COMMANDS 10 
#define PATH_MAX 255
#define STDIN 0
#define STDOUT 1
#define CHILDPID 0

/* To run valgrind write: valgrind --leak-check=full --show-reachable=yes ./MyShell [program parameters].*/

/*prints tests,strings and addresses.
 * to print int: testPrint(i,0,0,0);
 * to print string: testPrint(0,string,0,0);
 * to print address: testPrint(0,0,link,1);
 * IF WANT ADDRESS OF OTHER OBJECT JUST CHANGE THE SIG(int * address)
 */
void testPrint(int printIndex,char * string, int * address,int addressFlag){
  if(printIndex != 0)
    printf("TEST %d\n",printIndex);
  if(string != 0)
    printf("%s\n",string);
  if(addressFlag == 1)
    printf("%p\n",link);
}

/*receives the number of required pipes and returns an array of pipes*/
int ** createPipes(int nPipes){
  int ** pipes=(int**)malloc(nPipes*sizeof(int));/*cast to (int **) because nalloc returns (void *)*/
  int i;
  for(i=0 ; i< nPipes ; i++){
    pipes[i]=(int*)malloc(2*sizeof(int));
    if(pipe(pipes[i]) == -1)
      fprintf(stderr, "pipe failed\n");
  }
  return pipes;
}
/* receives an array of pipes and an integer indicating the size of the array.
 * The function releases all memory dedicated to the pipes
 */
void releasePipes(int **pipes, int nPipes){
  int i;
  for(i=0;i<nPipes;i++){
    free(pipes[i]); 
  }
  free(pipes);
}

int *leftPipe(int **pipes, cmdLine *pCmdLine) {
  if(pCmdLine->idx == 0)
    return 0;
  return pipes[(pCmdLine->idx)-1];
}

int * rightPipe(int ** pipes , cmdLine * pCmdLine){
  if(pCmdLine->next == 0 )
    return 0;
  return pipes[(pCmdLine->idx)];
}

/*redirects IO if necessary*/
void ioRedirect(cmdLine * pCmdLine){
  if(pCmdLine->inputRedirect != 0){
    close(STDIN);
    if(open(pCmdLine->inputRedirect , O_RDONLY) < 0){
      printf("Error, Can't open or read from file\n");
      exit(1);
    }
  }
  if(pCmdLine->outputRedirect != 0){
    close(STDOUT);
    if(open(pCmdLine->outputRedirect , O_WRONLY|O_CREAT ,0777) < 0){
      printf("Error, Can't create or write to file\n");
      exit(1);
    }
  }
}

/*receives a parsed line and invokes the command using the proper system call */
void execute(cmdLine * pCmdLine){
  int status;
  int fd[2];
  if(pipe(fd) == -1)
    fprintf(stderr, "pipe failed\n");
  pid_t child1 ;
  if((child1 = fork()) == -1){
    perror("fork");
    exit(1);
  }
  /*child process*/
  if(child1 == CHILDPID){
    ioRedirect(pCmdLine);/*redirects IO if necessary*/
    if( pCmdLine->next !=0){
      close(STDOUT);
      dup(fd[1]);
      close(fd[1]);
    }
    if(execvp((pCmdLine->arguments)[0],pCmdLine->arguments) == -1){
      exit(1); 
    }
  }
  if(child1> 0){
    if(pCmdLine->blocking == 1){
      waitpid(child1, &status, 0);
    }
    if( pCmdLine->next !=0){
      close(fd[1]);
      pCmdLine=pCmdLine->next;
      pid_t child2;
      if((child2 = fork()) == -1){
	perror("fork");
	exit(1);
      }
      if(child2 == CHILDPID){
	close(STDIN);
	dup(fd[0]);
	close(fd[0]);
	ioRedirect(pCmdLine);
	if(execvp((pCmdLine->arguments)[0],pCmdLine->arguments) == -1){
	  exit(1); 
	}
      }
      else{
	close(fd[0]);
	waitpid(child2, &status, 0);
      }
    }
  }
}


/*changes current directory*/
void changeDir(cmdLine * line){
  if(strcmp((line->arguments)[0],"cd")==0){
    if((chdir((line->arguments)[1])) == -1){
      perror("Error :");
    }
  }
}


/*prints command history*/
void printComHist(char ** commandHistory,int startIndex,int commandsCount){
  int i,j;
  for(i=0 ; i< commandsCount; i++){
    printf("%s%d%s","    ", i, "  ");
    for(j = 0; commandHistory[(startIndex = i)%MAX_COMMANDS][j] != '\0'; j++){
      printf("%c",commandHistory[(startIndex = i)%MAX_COMMANDS][j]);
    }
    printf("\n");
  }
}


/*adds commands to command history log*/
void addCommToLog(char ** commandHistory,char * lineBuf, int * startIndex, int * commandsCount){
  int i;
  if( (*commandsCount) < MAX_COMMANDS){
    commandHistory[(*commandsCount)] = malloc(MAX_LINE_SIZE);
    for( i = 0 ;  i<strlen(lineBuf) ; i++)
      commandHistory[(*commandsCount)][i] = lineBuf[i];
    commandHistory[(*commandsCount)][i] = '\0';
    (*commandsCount)++;
  }
  else{
    if( (*startIndex) == MAX_COMMANDS)
      (*startIndex) = 0;
    for (i=0 ; i<strlen(lineBuf) ; i++)
      commandHistory[(*startIndex)][i] = lineBuf[i];
    commandHistory[(*startIndex)][i] = '\0';
    (*startIndex)++;
  }
}

/*Display a prompt - the current working directory*/
void getCurWorkDir(char * dirname){
  if (getcwd(dirname, PATH_MAX) == NULL) { /*get current working directory*/
    fprintf(stderr, "Could not obtain current working directory.\n");
    exit(1);
  }
  else {
    printf("Current working directory: %s\n", dirname);
  }
}

/*finds a link in the linked list by name and returns it. if cannot be found returns NULL.*/
Pair * getLink(Pair * list , char * name){
  int found = 0;
  Pair * link = NULL;
  Pair * tmp = list;
  while( (found != 1) && tmp != NULL){
    if ( strcmp(tmp->name , name) == 0){
      return tmp;
    }
    tmp=tmp->next;
  }
  return link;
}

Pair * unassign(char * name, Pair * list){
  int found = 0;
  Pair * head = list;
  Pair * curr = list;
  Pair * prev = NULL;
  if(list == 0){
    printf("Error! cannot remove variable %s since it was not found.\n", name);
    return list;
  }
  while(found != 1 || curr != NULL){
    if(strncmp(curr->name,name,strlen(name)) == 0){/*found it*/
      if(prev == NULL){/* Fix beginning pointer. */
	list = curr->next;
	free(curr->name);
	free(curr->value);
	free(curr);
	return list;
      }
      else{
	free(curr->name);
	free(curr->value);
	prev->next = curr->next;
	found = 1;
	printf("variable %s has been removed successfully.\n", name);
	free(curr);
	return head;
      }
    }
    prev = curr;
    curr = curr->next;
  }
  if(found == 0)
    printf("Error! cannot remove variable %s since it was not found.\n", name);
  return head;
}

Pair * assign(char * name,char * value, Pair * list){
  Pair * temp;
  /*Link does not exist in the list- so create new one and append it*/
  if((temp = getLink(list,name)) == 0){
    /*TODO create new link and append() it to list*/
    Pair * newLink = malloc(sizeof(Pair)) ;
    newLink->name = malloc(strlen(name)+1);
    newLink->value = malloc(strlen(value) + 1);
    newLink->next = NULL;
    strcpy( newLink->name , name);
    strcpy( newLink->value , value);
    printf("value '%s' has been assigned to '%s' successfully.\n",value, name);
    return (append(list,newLink));
  }
  else{
    /*TODO edit the links value*/
    free(temp->value);
    temp->value = malloc(strlen(value)+1);
    strcpy( temp->value , value);
    printf("value '%s' has been reassigned to '%s' successfully.\n",value, name);
    return list;/*may be trouble here*/
  }
  
}

Pair * editPair(cmdLine * line, Pair * list){
  Pair * temp;
  if(strcmp("assign",line->arguments[0]) == 0){
    if(line->argCount != 3){
      printf("Error! invalid number of arguments\n");
      exit(1);
    }
    char * name = line->arguments[1];
    char * value = line->arguments[2];
    temp = assign(name, value, list);
    strcpy( line->arguments[0] , "  ");
  }
  else if(strcmp("unassign",line->arguments[0]) == 0){
    if(line->argCount != 2){
      printf("Error! invalid number of arguments\n");
      exit(1);
    }
    char * name = line->arguments[1];
    temp = unassign(name, list);
    return temp;
  }
  else if(strcmp("env",line->arguments[0]) == 0){
    if(line->argCount != 1){
      printf("Error! invalid number of arguments\n");
      exit(1);
    }
    temp = env(list);
  }
  return temp;
}

/*appends a link to the start of the list
 * returns the head of the list
 */
Pair * append(Pair * list , Pair * link){
  /*if list is empty return link*/
  if(list == 0){
    return link;
  }
  else{
    link->next = list;
  }
  return link;
}

/*prints all current associations in the environment*/
Pair * env(Pair * list){
  Pair * tmp = list;
  while(tmp != NULL){
    printf("< %s : %s >\n",tmp->name,tmp->value);
    tmp = tmp->next;
  }
  if(list == 0)
    printf("no 'name to value' association found\n");
  return list;
}

char * getValue(char * name, Pair * list){
  Pair * link =  getLink(list , name);
  if(link == 0){
    printf("Fatal Error! the name you have entered is not assigned to a value!!!\n");
    exit(1);
  }
  return link->value;
}

Pair * runCommand(Pair * list, char ** commandHistory, char * lineBuf, int * startIndex, int * commandsCount){
  Pair * temp = 0;
  cmdLine * line = parseCmdLines(lineBuf);
  if(strcmp("quit",lineBuf)==0){
    freeAll(commandHistory,list,commandsCount,startIndex);
    freeCmdLines(line);
    exit(0);
  }
  addCommToLog(commandHistory, lineBuf, startIndex, commandsCount);
  if(strcmp("log",lineBuf)==0){
    printComHist(commandHistory,*startIndex,*commandsCount);
    temp = list;
  }
  else{
    
    int i;
    for(i = 0; i<(line->argCount); i++){
      if((strncmp(line->arguments[i],"$",1)) == 0){
	/*replaceCmdArg(line,i,((line->arguments[i])+1));*/
	/*TODO also need to do a for loop to find $. it may be in i'th place*/
	char * nameToCopy = malloc(strlen(line->arguments[i]));
	strncpy(nameToCopy,((line->arguments[i])+1),strlen(line->arguments[i]));
	strcpy(line->arguments[i],getValue(nameToCopy,list));
	free(nameToCopy);
      }
    }
    temp = list;
    if((strcmp("assign",line->arguments[0])==0) || (strcmp("env",line->arguments[0])==0) || (strcmp("unassign",line->arguments[0])==0)){
      temp = editPair(line,list);
    }
    else{
      changeDir(line);
      execute(line);
      temp = list;
    }
    freeCmdLines(line);
  }
  if(temp == 0){
    return 0;
  }
  return temp;
}

Pair * checkReuseCommand(Pair * list,char ** commandHistory,char * lineBuf, int * startIndex, int * commandsCount){
  Pair * temp;
  if(lineBuf[0] == '!'){
    int num = lineBuf[1] - '0';
    if(num > (*commandsCount)){
      printf("command index out of bound, try again..\n");
    }
    else{
      char * reusedLine = commandHistory[num];
      temp = runCommand(list, commandHistory, reusedLine, startIndex, commandsCount);
    }
  }
  else{
    temp = runCommand(list, commandHistory, lineBuf, startIndex, commandsCount);
  }
  return temp;
}

void freeAll(char * commandHistory[],Pair * list,int * commandsCount,int * startIndex){
  int i;
  for(i=0;i<MAX_COMMANDS && commandHistory[i] != 0 ;i++){
    free(commandHistory[i]);
  }
  Pair * link = list;
  Pair * tmp;
  while(link != 0){
    tmp = link;
    link = link->next;
    free(tmp->name);
    free(tmp->value);
    free(tmp);
  }
  free(commandsCount);
  free(startIndex);
  list = 0;
  free(list);
}

void test(){
  cmdLine * testLine = parseCmdLines("ls | tee | cat | more");/*Parse "ls | tee | cat | more"*/
  int ** pipes = createPipes(3);/*Create an appropriate array of pipes.*/
  cmdLine * tmp = testLine;
  testPrint(0,tmp->arguments[0],0,0);
  if (!leftPipe(pipes,tmp))/*Validate that the left pipe of ls is NULL.*/
    testPrint(0,"left pipe of ls is NULL",0,0);
  tmp=tmp->next;
  testPrint(0,tmp->arguments[0],0,0);
  int i;
  for(i=0;i<1;i++){/*Print the file descriptors of both the left & right pipes of tee*/
    printf("%d  %d\n" , leftPipe(pipes , tmp)[0] , leftPipe(pipes , tmp)[1]);
    printf("%d  %d\n" , rightPipe(pipes , tmp)[0] , rightPipe(pipes , tmp)[1]);
    tmp=tmp->next;
  }
  tmp=tmp->next;
  testPrint(0,tmp->arguments[0],0,0);
  if (rightPipe(pipes,tmp) == 0)/*Validate that the right pipe of more is NULL.*/
    testPrint(0,"right pipe of ls is NULL",0,0);
  releasePipes(pipes,3);/*Release the pipes.*/
  freeCmdLines(testLine);
}

int main(void){
  test();
  char dirname[PATH_MAX + 1]; /* To be passed to getcwd system call.*/
  char lineBuf[MAX_LINE_SIZE + 1];
  int *startIndex = (int *)malloc(sizeof(int));
  int *commandsCount = (int *)malloc(sizeof(int));
  *startIndex = 0;
  *commandsCount = 0;
  char * commandHistory[MAX_COMMANDS];
  Pair * list = 0;
  while(1){
    getCurWorkDir(dirname);
    if(!gets(lineBuf))
      exit(0);
    if(strlen(lineBuf) > 1)
      list = checkReuseCommand(list,commandHistory, lineBuf, startIndex, commandsCount);
    else
      printf("incorrect input\n");
  }
  return 0;
}

