typedef struct Pair{
  char *  name; /* a variable name that appears in the command line - example: "i" */
  char *  value;	/* value of name. example: when command is "$i" output is "hello" */
  struct Pair * next;	/* next stringPair in chain */
} Pair;

/**************HEADERS****************/
void execute(cmdLine * line);
Pair * getLink(Pair * list , char * name);
Pair * assign(char * name,char * value, Pair * list);
Pair * append(Pair * list , Pair * link);
Pair * env(Pair * list);
void changeDir(cmdLine * line);
char * getValue(char * name, Pair * list);
void freeAll(char * commandHistory[],Pair * list,int * commandsCount,int * startIndex);
void testPrint(int printIndex,char * string, int * address,int addressFlag);
void ioRedirect(cmdLine * line);
int **createPipes(int nPipes) ;
void releasePipes(int **pipes, int nPipes);
int *leftPipe(int **pipes, cmdLine *pCmdLine) ;
int *rightPipe(int **pipes, cmdLine *pCmdLine) ;
/**************HEADERS****************/ 
