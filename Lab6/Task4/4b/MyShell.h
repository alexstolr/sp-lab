

typedef struct Pair{
  char *  name;
  char *  value;
  struct Pair * next;
} Pair;


void execute(cmdLine * line);
Pair * getLink(Pair * list , char * name);
Pair * assign(char * name,char * value, Pair * list);
Pair * append(Pair * list , Pair * link);
Pair * env(Pair * list);
void changeDir(cmdLine * line);
char * getValue(char * name, Pair * list);
void freeAll(char * commandHistory[],Pair * list,int * commandsCount,int * startIndex);
void testPrint(int printIndex,char * string, int * address,int addressFlag);
void ioRedirect(cmdLine * line);
int **createPipes(int nPipes) ;
void releasePipes(int **pipes, int nPipes);
int *leftPipe(int **pipes, cmdLine *pCmdLine) ;
int *rightPipe(int **pipes, cmdLine *pCmdLine) ; 
