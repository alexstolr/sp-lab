#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <sys/wait.h>
#include <sys/types.h>
#include <errno.h>

#define STDIN 0
#define STDOUT 1
#define CHILDPID 0

/*ls -l gives info about all files in the directory.
 * a file and its info in each line
 * 
 * tail -n 2 [filename]
 * reads last 2 lines from filename. if filename is absent
 * it will read from stdin
 */

void print(int printIndex){
  if(printIndex == 1)
    printf("TEST %d\n",printIndex);
}

int main(int argc , char **argv){
  int fd[2];
  int status;
  pid_t child1;
  /*pipefd[0] refers to the  read  end  of  the  pipe.
   *pipefd[1]  refers to the write end of the pipe.*/
  if(pipe(fd) == -1){ 
    fprintf(stderr, "pipe failed\n");
  }
  if((child1 = fork()) == -1){
    perror("fork");
    exit(1);
  }
  if(child1 == CHILDPID){
    close(STDOUT);
    dup(fd[STDOUT]);
    close(fd[STDOUT]);
    char * args1[3] = {"ls","-l",0};
    if(execvp(args1[0],args1) == -1){
      exit(errno);
    }
  }
  /*parent process*/
  else{
    close(fd[STDOUT]);
    pid_t child2;
    if((child2 = fork()) == -1){
      perror("fork");
      exit(1);
    }
    if(child2 == CHILDPID){
      close(STDIN);
      dup(fd[STDIN]);
      close(fd[STDIN]);
      char * args2[4] = {"tail","-n","2",0};
      if(execvp(args2[0],args2) == -1){
	exit(errno);
      }
    }
    else{
      close(fd[STDIN]);
      waitpid(child1, &status, 0);
      waitpid(child2, &status, 0);
    }
  }
  return 0;
}
