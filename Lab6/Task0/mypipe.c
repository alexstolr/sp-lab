#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <sys/types.h>
#include <errno.h>

#define STDIN 0
#define STDOUT 1
#define CHILDPID 0

int main(){
  int fd[2];
  int status;
  pid_t pid;
  char * string = "hello";
  char readbuffer[strlen(string)+1];
  /*the pipe() system call takes a single argument,
   * which is an array of two integers, and if successful,
   * the array will contain two new file descriptors to be used for the pipeline.
   * After creating a pipe, 
   * the process typically spawns a new process (remember the child inherits open file descriptors)
   */
  pipe(fd);
  /*The first integer in the array (element 0)
   * is set up and opened for reading,
   * while the second integer (element 1)
   * is set up and opened for writing.
   * Visually speaking, the output of fd1 becomes the input for fd0.
   */
  if((pid = fork()) == -1){
    perror("fork");
    exit(1);
  }
  
  if(pid == CHILDPID){
    /* Child process closes up input side of pipe(closes reading option) */
    close(fd[STDIN]);
    
    /* Send "string" through the output side of pipe */
    write(fd[STDOUT], string, (strlen(string)+1));
    exit(0);
  }
  else{
    /* Parent process closes up output side of pipe(closes writing option) */
    close(fd[1]);
    
    /* Read in a string from the pipe */
    if((read(fd[0], readbuffer, sizeof(readbuffer)))== -1){
      printf("Oh dear, something went wrong with read()! %s\n", strerror(errno));
      
    }
    printf("Received string: %s\n", readbuffer);
  }
  return 0;
}
