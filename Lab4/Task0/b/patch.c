#include "util.h"

#define SYS_OPEN 5
#define SYS_CLOSE 6
#define SYS_READ 3
#define SYS_WRITE 4

#define STDIN 0
#define STDOUT 1

#define O_RDONLY 0
#define O_WRONLY 1
#define O_RDRW 2
#define O_CREAT 64

int main (int argc , char* argv[], char* envp[]){
  int size = 7416; /*7416 is the size of the file*/
  char buff[size];
  int fileToRead;
  int fileToWrite;
  
  if(argc != 2){
    system_call(SYS_WRITE, STDOUT, "Bad input.\n", strlen("Bad input.\n"));
    return 1;
  }
  char * name = argv[1];
  int nameLen = strlen(name);
  
  fileToRead = system_call(SYS_OPEN,"greeting",O_RDONLY, 0777);/*open file 'greeting' to reading*/
  fileToWrite = system_call(SYS_OPEN,"greeting2", O_CREAT | O_RDRW, 0777); /*create 'greeting2' & can write*/
  
  system_call(SYS_READ, fileToRead, buff, size); /*reads from 'greeting' to buffer*/
  system_call(SYS_CLOSE,fileToRead);
  int i = 0;
  char * shira = "Shira";
  for(i=0; i < size; i++){ /*TODO need to use lseek here in order to change name using a loop. change it if have time*/
    if ( strncmp(buff+i, shira, 5) == 0){
      system_call(SYS_WRITE, STDOUT, "succesfuly replaced name\n!!", strlen("succesfuly replaced name\n!!"));
      *(buff+(i++)) = 'm';
      *(buff+(i++)) = 'i';
      *(buff+(i++)) = 'r';
      *(buff+(i++)) = 'a';
      *(buff+(i++)) = ' ';
    }
  }
  system_call(SYS_WRITE, fileToWrite, buff, size);
  system_call(SYS_CLOSE,fileToWrite);  
  return 0;
}


