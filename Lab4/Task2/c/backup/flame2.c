
#include "util.h"

#define SYS_OPEN 5
#define SYS_CLOSE 6
#define SYS_READ 3
#define SYS_WRITE 4

#define STDIN 0
#define STDOUT 1

#define O_RDONLY 0s
#define O_WRONLY 1
#define O_RDRW 2
#define O_CREAT 64
#define O_APPEND 1024

#define BUF_SIZE 8192
#define SYS_GETDENTS 141

#define SYS_EXIT 1

void errorSC(int errorConst){
  system_call(SYS_WRITE,STDOUT,"ERROR! exiting program...\n", strlen("ERROR! exiting program...\n"));
  system_call(SYS_EXIT,0x55);
}

struct linux_dirent  {
  unsigned long  d_ino;     /* Inode number */
  unsigned long  d_off;     /* Offset to next linux_dirent */
  unsigned short d_reclen;  /* Length of this linux_dirent */
  char           d_name[];  /* Filename (null-terminated) */
};

void infection(void);/*prints to the screen the message "Hello, Infected File".*/


/*opens the file named in its argument,
 * and adds the executable code from "code_start" to "code_end" after the end of that file,
 * and closes the file.
 */
void infector(char *fileName);


int main (int argc , char* argv[], char* envp[]){
  
  int fd, nread;
  /*char * nread_str;*/
  char buf[BUF_SIZE];
  struct linux_dirent *d;
  char d_type;
  int pFlag = 0;
  int aFlag = 0;
  char * prefix;
  int i;
  if(argc == 1){
    system_call(SYS_WRITE,STDOUT,"Error, no arguments inserted\n",strlen("Error, no arguments inserted\n"));
    errorSC(SYS_EXIT);
  }
  for(i = 1; i < argc ; i++){
    if(strcmp(argv[i],"-p") == 0){
      if(pFlag == 1 || aFlag == 1){
	system_call(SYS_WRITE,STDOUT,"Error, program supports only one prefix\n",strlen("Error, program supports only one prefix\n"));
	errorSC(SYS_EXIT);
      }
      else{
	pFlag=1;
	prefix = argv[++i];
      }
    }
    else if(strcmp(argv[i],"-a") == 0){
      if(aFlag == 1 || pFlag == 1){
	system_call(SYS_WRITE,STDOUT,"Error, program supports only one prefix\n",strlen("Error, program supports only one prefix\n"));
	errorSC(SYS_EXIT);
      }
      else{
	aFlag=1;
	prefix = argv[++i];
      }
    }
    else{
      system_call(SYS_WRITE,STDOUT,"wrong input\n",strlen("wrong input\n"));
      i++;
      return 0;
    }
  }
  
  if((fd=system_call(SYS_OPEN,".", 0 , 0)) == -1){
    errorSC(SYS_EXIT);
  }
  if((nread = system_call(SYS_GETDENTS, fd, buf, BUF_SIZE)) <= 0){
    errorSC(SYS_EXIT);
  }
  
  system_call(SYS_WRITE,1,"Flame 2 strikes!\n\n",strlen("Flame 2 strikes!\n\n"));
  
  /*nread_str=itoa(nread);*/
  int bpos;
  if(pFlag == 1){
    for (bpos = 0 ; bpos < nread ; bpos += d->d_reclen) {
      d = (struct linux_dirent *) (buf + bpos);
      d_type = *(buf + bpos + d->d_reclen - 1);
      if(strncmp(d->d_name, prefix, strlen(prefix)) == 0){
	system_call(SYS_WRITE,STDOUT, d->d_name,strlen(d->d_name));
	system_call(SYS_WRITE,STDOUT,"\n",1);
      }
    }
  }
  if(aFlag == 1){
    for (bpos = 0 ; bpos < nread ; bpos += d->d_reclen) {
      d = (struct linux_dirent *) (buf + bpos);
      d_type = *(buf + bpos + d->d_reclen - 1);
      if(strncmp(d->d_name, prefix, strlen(prefix)) == 0){
	system_call(SYS_WRITE,STDOUT, d->d_name,strlen(d->d_name));
	system_call(SYS_WRITE,STDOUT,"\n",1);
      }
      infector(d->d_name);
    }
  }
  
  system_call(SYS_CLOSE,fd);
  return 0;
}

void infection(void){
  system_call(SYS_WRITE,STDOUT,"Hello, Infected File\n",strlen("Hello, Infected File\n"));
}

void infector(char *fileName){
  int fd;
  if((fd = system_call(SYS_OPEN,fileName,O_APPEND | O_RDRW,0)) == -1){
    errorSC(SYS_EXIT);
  }
  else{
    system_call(SYS_WRITE,fd,"Hello, Infected File", strlen("Hello, Infected File"));
    infection();
  }
}




