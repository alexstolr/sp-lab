#include "util.h"

#define SYS_OPEN 5
#define SYS_CLOSE 6
#define SYS_WRITE 4

#define STDOUT 1

#define BUF_SIZE 8192
#define SYS_GETDENTS 141

#define SYS_ERROR 1

extern int system_call();

void sysWrite(char * string){
  system_call(SYS_WRITE,STDOUT,string,strlen(string));
}

void sysErr(char * errMSg){
  sysWrite(errMSg);
  system_call(SYS_ERROR,0x55); 
}

struct linux_dirent  {
  unsigned long  d_ino;     /* Inode number */
  unsigned long  d_off;     /* Offset to next linux_dirent */
  unsigned short d_reclen;  /* Length of this linux_dirent */
  char           d_name[];  /* Filename (null-terminated) */
};

int main (int argc , char* argv[], char* envp[]){

  int fd, nread;
  char buf[BUF_SIZE];
  struct linux_dirent *d;
  
  if((fd=system_call(SYS_OPEN,".", 0 , 0)) == -1)
    sysErr("Error opening folder\n");
  if((nread = system_call(SYS_GETDENTS, fd, buf, BUF_SIZE)) <= 0)
    sysErr("Error reading from 'fd' into 'buf'\n");   
  
  sysWrite("\n\nFlame 2 strikes!            Flame 2 strikes!            Flame 2 strikes!\n\n");
  int bpos;
  for (bpos = 0 ; bpos < nread ; bpos += d->d_reclen) {
    d = (struct linux_dirent *) (buf + bpos);
    sysWrite(d->d_name);
    sysWrite("\n");
  }
  system_call(SYS_CLOSE,fd);
  sysWrite("\n\nFlame 2 strikes!            Flame 2 strikes!            Flame 2 strikes!\n\n");
  return 0;
}






