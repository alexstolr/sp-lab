#include "util.h"

#define SYS_OPEN 5
#define SYS_CLOSE 6 
#define SYS_READ 3
#define SYS_WRITE 4

#define SYS_ERROR 1

#define STDIN 0
#define STDOUT 1

#define O_RDONLY 0
#define O_WRONLY 1
#define O_CREAT 64

extern int system_call();

void sysErr(char * errMSg){
  system_call(SYS_WRITE,STDOUT,errMSg,strlen(errMSg));
  system_call(SYS_ERROR,0x55); 
}

void delimSplit(int readSource, int writeDest){
  char buff;
  char nLine = '\n';
  while((system_call(SYS_READ,readSource, &buff, 1)) > 0){
    if(buff == ';' || buff == ':' || buff == ',' || buff == '.'){
      system_call(SYS_WRITE,writeDest,&buff,1);
      system_call(SYS_WRITE,writeDest,&nLine,1);
    }
    else{
      system_call(SYS_WRITE,writeDest,&buff,1);
    } 
  }
}

int main (int argc , char* argv[], char* envp[]){
  int oFlag = 0, iFlag = 0;
  char * inputFileName;
  char * outputFileName;
  int fileToRead;
  int fileToWrite;
  int i;
  for(i=1; i<argc; i++){
    if(strcmp(argv[i],"-o") == 0){
      if(oFlag == 1){
	sysErr("Error, program supports only one output file\n");
      }
      else{
	oFlag=1;
	outputFileName = argv[++i];
	if((fileToWrite = system_call(SYS_OPEN,outputFileName, O_CREAT | O_WRONLY, 0644)) < 0)
	  sysErr("Can't create or write to file\n");
      }
    }
    else if(strcmp(argv[i],"-i") == 0){
      if(iFlag == 1){
	sysErr("Error, program supports only one input file\n");
      }
      else{
	iFlag=1;
	inputFileName = argv[++i];
	if((fileToRead = system_call(SYS_OPEN,inputFileName,O_RDONLY, 0777)) < 0){
	  sysErr("Can't open or read from file\n");
	}
      }
    }
    else{
      sysErr("wrong input\n");
    }
  }
  
  if(iFlag == 1){
    if(oFlag == 1)
      delimSplit(fileToRead,fileToWrite);
    if(oFlag == 0)
      delimSplit(fileToRead,STDOUT);
  }
  if(iFlag == 0){
    if(oFlag == 1)
      delimSplit(STDIN,fileToWrite);
    if(oFlag == 0)
      delimSplit(STDIN,STDOUT);
  }
  
  system_call(SYS_CLOSE,fileToRead);
  system_call(SYS_CLOSE,fileToWrite);
  return 0;
}






