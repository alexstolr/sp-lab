#include "util.h"

#define SYS_READ 3
#define SYS_WRITE 4

#define STDIN 0
#define STDOUT 1
extern int system_call();

/*read the whole string  and write one character at a time!*/
/*
int main(){
  char buff[256];
  char nLine[1] = "\n";
  if((system_call(SYS_READ,STDIN, buff, 256)) > 0){
    int size = strlen(buff);
    int i;
    for(i = 0; i < size ; i++){
      if(*(buff + i) == ';' || *(buff + i) == ':' || *(buff + i) == ',' || *(buff + i) == '.'){
	system_call(SYS_WRITE,STDOUT,buff + i,1);
	system_call(SYS_WRITE,STDOUT,nLine,1);
      }
      else{
	system_call(SYS_WRITE,STDOUT,buff + i,1);
      }
    }
  }
  return 0;
}
*/

/*read and write one character at a time*/
int main(){
  char buff;
  char nLine = '\n';
  while((system_call(SYS_READ,STDIN, &buff, 1)) > 0){
   if(buff == ';' || buff == ':' || buff == ',' || buff == '.'){
	system_call(SYS_WRITE,STDOUT,&buff,1);
	system_call(SYS_WRITE,STDOUT,&nLine,1);
      }
      else{
	system_call(SYS_WRITE,STDOUT,&buff,1);
      } 
  }
  return 0;
}