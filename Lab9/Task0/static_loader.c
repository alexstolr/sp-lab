#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <elf.h>
#include "./functions.h"
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <sys/mman.h>
#include <fcntl.h> /*open*/

/*void function(Elf32_Phdr * progHeader,int i){
}*/

int foreach_phdr(void *map_start, void (*func)(Elf32_Phdr *,int), int arg){
   Elf32_Ehdr * header = (Elf32_Ehdr *) map_start;
   if((checkELF(4,header)) == 0){
    printf("Error, the file you have chosen is not of 'ELF32' format!\n");
    exit(1);
  }
   int num_of_progHeaders = header->e_phnum;
   Elf32_Phdr * progHeader = (Elf32_Phdr *)(map_start+header->e_phoff);
   int i;
  for(i = 0; i< num_of_progHeaders ; i++){
    printf("Program header number %d at address %#08x\n",i,(progHeader[i]).p_paddr );
    func(progHeader,i);/*TODO change this if needed*/
  }
    
  
  return 0;
}


int main(int argc, char ** argv){
  checkArgs(argc);
  int fd;
  void *map_start;
  struct stat fd_stat;
  if( (fd = open(argv[1], O_RDWR)) < 0 ){
    perror("error in open");
    exit(-1);
  }
  if( fstat(fd, &fd_stat) != 0 ){
    perror("stat failed");
    exit(-1);
  }
  if( (map_start = mmap(0, fd_stat.st_size, PROT_READ | PROT_WRITE , MAP_SHARED, fd, 0)) < 0 ){
    perror("mmap failed");
    exit(-4);
  }
  foreach_phdr(map_start,function,0);
  
  /* now, we unmap the file */
  munmap(map_start, fd_stat.st_size);
  
  return 0;
}
