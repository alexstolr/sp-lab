#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <elf.h>
#include "./functions.h"
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <sys/mman.h>
#include <fcntl.h> /*open*/

void function(Elf32_Phdr * progHeader,int i){
}

void checkFlags(Elf32_Phdr * progHeader,int i){
  if(progHeader[i].p_flags == PF_X){
    printf(" %-04s","E");
  }
  if(progHeader[i].p_flags == PF_W){
    printf(" %-04s","W");
  }
  if(progHeader[i].p_flags == PF_W+PF_X){
    printf(" %-04s","W E");
  }
  if(progHeader[i].p_flags == PF_R){
    printf(" %-04s","R");
  }
  if(progHeader[i].p_flags == PF_R+PF_X){
    printf(" %-04s","R E");
  }
  if(progHeader[i].p_flags == PF_R+PF_W){
    printf(" %-04s","RW");
  }
  if(progHeader[i].p_flags == PF_R+PF_W+PF_X){
    printf(" %-04s","R W E");
  }
}

void task1a(Elf32_Phdr * progHeader,int i){
  if((progHeader[i].p_type) == PT_NULL){
      printf("  %-15s%#08x %#010x %#010x %#07x %#07x","",progHeader[i].p_offset,progHeader[i].p_vaddr,progHeader[i].p_paddr,progHeader[i].p_filesz,progHeader[i].p_memsz);
      checkFlags(progHeader,i);
      printf("%#x",progHeader[i].p_align);
  }
  else if((progHeader[i].p_type) == PT_LOAD){
      printf("  %-15s%#08x %#010x %#010x %#07x %#07x","LOAD",progHeader[i].p_offset,progHeader[i].p_vaddr,progHeader[i].p_paddr,progHeader[i].p_filesz,progHeader[i].p_memsz);
      checkFlags(progHeader,i);
      printf("%#x",progHeader[i].p_align);
  }
  else if((progHeader[i].p_type) == PT_DYNAMIC){
      printf("  %-15s%#08x %#010x %#010x %#07x %#07x","DYNAMIC",progHeader[i].p_offset,progHeader[i].p_vaddr,progHeader[i].p_paddr,progHeader[i].p_filesz,progHeader[i].p_memsz);
      checkFlags(progHeader,i);
      printf("%#x",progHeader[i].p_align);
  }
  else if((progHeader[i].p_type) == PT_INTERP){
      printf("  %-15s%#08x %#010x %#010x %#07x %#07x","INTERP",progHeader[i].p_offset,progHeader[i].p_vaddr,progHeader[i].p_paddr,progHeader[i].p_filesz,progHeader[i].p_memsz);
      checkFlags(progHeader,i);
      printf("%#x",progHeader[i].p_align);
  }
  else if((progHeader[i].p_type) == PT_NOTE){
      printf("  %-15s%#08x %#010x %#010x %#07x %#07x","NOTE",progHeader[i].p_offset,progHeader[i].p_vaddr,progHeader[i].p_paddr,progHeader[i].p_filesz,progHeader[i].p_memsz);
      checkFlags(progHeader,i);
      printf("%#x",progHeader[i].p_align);
  }
  else if((progHeader[i].p_type) == PT_SHLIB){
      printf("  %-15s%#08x %#010x %#010x %#07x %#07x","SHLIB",progHeader[i].p_offset,progHeader[i].p_vaddr,progHeader[i].p_paddr,progHeader[i].p_filesz,progHeader[i].p_memsz);
      checkFlags(progHeader,i);
      printf("%#x",progHeader[i].p_align);
  }
  else if((progHeader[i].p_type) == PT_PHDR){
      printf("  %-15s%#08x %#010x %#010x %#07x %#07x","PHDR",progHeader[i].p_offset,progHeader[i].p_vaddr,progHeader[i].p_paddr,progHeader[i].p_filesz,progHeader[i].p_memsz);
      checkFlags(progHeader,i);
      printf("%#x",progHeader[i].p_align);
  }
  else if((progHeader[i].p_type) == PT_LOPROC){
      printf("  %-15s%#08x %#010x %#010x %#07x %#07x","LOPROC",progHeader[i].p_offset,progHeader[i].p_vaddr,progHeader[i].p_paddr,progHeader[i].p_filesz,progHeader[i].p_memsz);
      checkFlags(progHeader,i);
      printf("%#x",progHeader[i].p_align);
  }
  else if((progHeader[i].p_type) == PT_HIPROC){
      printf("  %-15s%#08x %#010x %#010x %#07x %#07x","HIPROC",progHeader[i].p_offset,progHeader[i].p_vaddr,progHeader[i].p_paddr,progHeader[i].p_filesz,progHeader[i].p_memsz);
      checkFlags(progHeader,i);
      printf("%#x",progHeader[i].p_align);
  }
  
  printf("\n");
  
}

int foreach_phdr(void *map_start, void (*func)(Elf32_Phdr *,int), int arg){
   Elf32_Ehdr * header = (Elf32_Ehdr *) map_start;
   if((checkELF(4,header)) == 0){
    printf("Error, the file you have chosen is not of 'ELF32' format!\n");
    exit(1);
  }
   int num_of_progHeaders = header->e_phnum;
   Elf32_Phdr * progHeader = (Elf32_Phdr *)(map_start+header->e_phoff);
   int i;
   printf("Program Headers:\n  Type           Offset   VirtAddr   PhysAddr   FileSiz MemSiz  Flg Align\n");
  for(i = 0; i< num_of_progHeaders ; i++){
    /*printf("Program header number %d at address %#08x\n",i,(progHeader[i]).p_paddr );*/
    task1a(progHeader,i);
    /*printf("-15%s\n",(progHeader[i].p_type).PT_PHDR);*/
    func(progHeader,i);/*TODO change this if needed*/
  }
  return 0;
}




int main(int argc, char ** argv){
  checkArgs(argc);
  int fd;
  void *map_start;
  struct stat fd_stat;
  if( (fd = open(argv[1], O_RDWR)) < 0 ){
    perror("error in open");
    exit(-1);
  }
  if( fstat(fd, &fd_stat) != 0 ){
    perror("stat failed");
    exit(-1);
  }
  if( (map_start = mmap(0, fd_stat.st_size, PROT_READ | PROT_WRITE , MAP_SHARED, fd, 0)) < 0 ){
    perror("mmap failed");
    exit(-4);
  }
  foreach_phdr(map_start,function,0);
  
  /* now, we unmap the file */
  munmap(map_start, fd_stat.st_size);
  
  return 0;
}
