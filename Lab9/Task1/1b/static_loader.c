#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <elf.h>
#include "./functions.h"
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <sys/mman.h>
#include <fcntl.h> /*open*/

void function(Elf32_Phdr * progHeader,int i){
  /*printf("Program header number %d at address %#08x\n",i,(progHeader[i]).p_paddr );*/
}

void checkFlags(Elf32_Phdr * progHeader,int i){
  if(progHeader[i].p_flags == PF_X){
    printf(" %-04s","E");
  }
  if(progHeader[i].p_flags == PF_W){
    printf(" %-04s","W");
  }
  if(progHeader[i].p_flags == PF_W+PF_X){
    printf(" %-04s","W E");
  }
  if(progHeader[i].p_flags == PF_R){
    printf(" %-04s","R");
  }
  if(progHeader[i].p_flags == PF_R+PF_X){
    printf(" %-04s","R E");
  }
  if(progHeader[i].p_flags == PF_R+PF_W){
    printf(" %-04s","RW");
  }
  if(progHeader[i].p_flags == PF_R+PF_W+PF_X){
    printf(" %-04s","R W E");
  }
}

void printfProtFlag(Elf32_Phdr * progHeader,int i){
  if((progHeader[i].p_type) == PT_LOAD){
    printf("\t%s","protection flags: ");
  int protExec = 0,protRead = 0,protWrite = 0,count = 0;
  if(progHeader[i].p_flags == PF_X){
    protExec = 1;
    count++;
  }
  if(progHeader[i].p_flags == PF_W){
    protWrite = 1;
    count++;
  }
  if(progHeader[i].p_flags == PF_W+PF_X){
    protWrite = 1;
    protExec = 1;
    count = count + 2;
  }
  if(progHeader[i].p_flags == PF_R){
    protRead = 1;
    count++;
  }
  if(progHeader[i].p_flags == PF_R+PF_X){
    protRead = 1;
    protExec = 1;
    count = count + 2;
  }
  if(progHeader[i].p_flags == PF_R+PF_W){
    protRead = 1;
    protWrite = 1;
    count = count + 2;
  }
  if(progHeader[i].p_flags == PF_R+PF_W+PF_X){
    protRead = 1;
    protWrite = 1;
    protExec = 1;
    count = count + 3;
  }
  if(count == 0)
    printf("%s","PROT_NONE");
  if(count == 1){
    if(protRead==1)
      printf("%s","PROT_READ");
    if(protWrite==1)
      printf("%s","PROT_WRITE");
    if(protExec==1)
      printf("%s","PROT_EXEC");
  }
  else if(count == 2){
    if(protRead==1 && protWrite==1)
      printf("%s","PROT_READ | PROT_WRITE");
    if(protExec==1 && protWrite==1)
      printf("%s","PROT_EXEC | PROT_WRITE");
    if(protExec==1 && protRead==1)
      printf("%s","PROT_EXEC | PROT_READ");
  }
  else
    printf("%s","PROT_READ | PROT_WRITE | PROT_EXEC");
  }
  
}

char * checkType(Elf32_Phdr * progHeader,int i){
  if((progHeader[i].p_type) == PT_NULL){
    return "";
  }
  if((progHeader[i].p_type) == PT_LOAD){
    return "LOAD";
  }
  if((progHeader[i].p_type) == PT_DYNAMIC){
    return "DYNAMIC";
  }
  if((progHeader[i].p_type) == PT_INTERP){
    return "INTERP";
  }
  if((progHeader[i].p_type) == PT_NOTE){
    return "NOTE";
  }
  if((progHeader[i].p_type) == PT_SHLIB){
    return "SHLIB";
  }
  if((progHeader[i].p_type) == PT_PHDR){
    return "PHDR";
  }
  if((progHeader[i].p_type) == PT_LOPROC){
    return "LOPROC";
  }
  if((progHeader[i].p_type) == PT_HIPROC){
    return "HIPROC";
  }
  return "";
}

void printTask1a(Elf32_Phdr * progHeader,int i){
  if(strcmp(checkType(progHeader,i),"")){
    printf("  %-10s%#08x %#010x %#010x %#07x %#07x",checkType(progHeader,i),progHeader[i].p_offset,progHeader[i].p_vaddr,progHeader[i].p_paddr,progHeader[i].p_filesz,progHeader[i].p_memsz);
  checkFlags(progHeader,i);
  printf("%#x",progHeader[i].p_align);
  printfProtFlag(progHeader,i);
  printf("\n");
  }
}

/*void task1a(Elf32_Phdr * progHeader,int i){
  if((progHeader[i].p_type) == PT_NULL){
    printf("  %-10s%#08x %#010x %#010x %#07x %#07x","",progHeader[i].p_offset,progHeader[i].p_vaddr,progHeader[i].p_paddr,progHeader[i].p_filesz,progHeader[i].p_memsz);
    checkFlags(progHeader,i);
    printf("%#x",progHeader[i].p_align);
    printfProtFlag(progHeader,i);
  }
  else if((progHeader[i].p_type) == PT_LOAD){
    printf("  %-10s%#08x %#010x %#010x %#07x %#07x","LOAD",progHeader[i].p_offset,progHeader[i].p_vaddr,progHeader[i].p_paddr,progHeader[i].p_filesz,progHeader[i].p_memsz);
    checkFlags(progHeader,i);
    printf("%#x",progHeader[i].p_align);
    printfProtFlag(progHeader,i);
  }
  else if((progHeader[i].p_type) == PT_DYNAMIC){
    printf("  %-10s%#08x %#010x %#010x %#07x %#07x","DYNAMIC",progHeader[i].p_offset,progHeader[i].p_vaddr,progHeader[i].p_paddr,progHeader[i].p_filesz,progHeader[i].p_memsz);
    checkFlags(progHeader,i);
    printf("%#x",progHeader[i].p_align);
    printfProtFlag(progHeader,i);
  }
  else if((progHeader[i].p_type) == PT_INTERP){
    printf("  %-10s%#08x %#010x %#010x %#07x %#07x","INTERP",progHeader[i].p_offset,progHeader[i].p_vaddr,progHeader[i].p_paddr,progHeader[i].p_filesz,progHeader[i].p_memsz);
    checkFlags(progHeader,i);
    printf("%#x",progHeader[i].p_align);
    printfProtFlag(progHeader,i);
  }
  else if((progHeader[i].p_type) == PT_NOTE){
    printf("  %-10s%#08x %#010x %#010x %#07x %#07x","NOTE",progHeader[i].p_offset,progHeader[i].p_vaddr,progHeader[i].p_paddr,progHeader[i].p_filesz,progHeader[i].p_memsz);
    checkFlags(progHeader,i);
    printf("%#x",progHeader[i].p_align);
    printfProtFlag(progHeader,i);
  }
  else if((progHeader[i].p_type) == PT_SHLIB){
    printf("  %-10s%#08x %#010x %#010x %#07x %#07x","SHLIB",progHeader[i].p_offset,progHeader[i].p_vaddr,progHeader[i].p_paddr,progHeader[i].p_filesz,progHeader[i].p_memsz);
    checkFlags(progHeader,i);
    printf("%#x",progHeader[i].p_align);
    printfProtFlag(progHeader,i);
  }
  else if((progHeader[i].p_type) == PT_PHDR){
    printf("  %-10s%#08x %#010x %#010x %#07x %#07x","PHDR",progHeader[i].p_offset,progHeader[i].p_vaddr,progHeader[i].p_paddr,progHeader[i].p_filesz,progHeader[i].p_memsz);
    checkFlags(progHeader,i);
    printf("%#x",progHeader[i].p_align);
    printfProtFlag(progHeader,i);
  }
  else if((progHeader[i].p_type) == PT_LOPROC){
    printf("  %-10s%#08x %#010x %#010x %#07x %#07x","LOPROC",progHeader[i].p_offset,progHeader[i].p_vaddr,progHeader[i].p_paddr,progHeader[i].p_filesz,progHeader[i].p_memsz);
    checkFlags(progHeader,i);
    printf("%#x",progHeader[i].p_align);
    printfProtFlag(progHeader,i);
  }
  else if((progHeader[i].p_type) == PT_HIPROC){
    printf("  %-10s%#08x %#010x %#010x %#07x %#07x","HIPROC",progHeader[i].p_offset,progHeader[i].p_vaddr,progHeader[i].p_paddr,progHeader[i].p_filesz,progHeader[i].p_memsz);
    checkFlags(progHeader,i);
    printf("%#x",progHeader[i].p_align);
    printfProtFlag(progHeader,i);
  }
  printf("\n");
}*/

int foreach_phdr(void *map_start, void (*func)(Elf32_Phdr *,int), int arg){
  Elf32_Ehdr * header = (Elf32_Ehdr *) map_start;
  if((checkELF(4,header)) == 0){
    printf("Error, the file you have chosen is not of 'ELF32' format!\n");
    exit(1);
  }
  int num_of_progHeaders = header->e_phnum;
  Elf32_Phdr * progHeader = (Elf32_Phdr *)(map_start+header->e_phoff);
  int i;
  printf("Program Headers:\n  Type      Offset   VirtAddr   PhysAddr   FileSiz MemSiz  Flg Align\n");
  for(i = 0; i< num_of_progHeaders ; i++){
    printTask1a(progHeader,i);
    /*task1a(progHeader,i);*/
    /*printf("-15%s\n",(progHeader[i].p_type).PT_PHDR);*/
    func(progHeader,i);/*TODO change this if needed*/
  }
  return 0;
}

/*
 * The  prot  argument describes the desired memory protection of the mapping (and must not conflict with the
 *       open mode of the file).  It is either PROT_NONE or the bitwise OR of one or more of the following flags:
 * 
 *       PROT_EXEC  Pages may be executed.
 * 
 *       PROT_READ  Pages may be read.
 * 
 *       PROT_WRITE Pages may be written.
 * 
 *       PROT_NONE  Pages may not be accessed.
 */





int main(int argc, char ** argv){
  checkArgs(argc);
  int fd;
  void *map_start;
  struct stat fd_stat;
  if( (fd = open(argv[1], O_RDWR)) < 0 ){
    perror("error in open");
    exit(-1);
  }
  if( fstat(fd, &fd_stat) != 0 ){
    perror("stat failed");
    exit(-1);
  }
  if( (map_start = mmap(0, fd_stat.st_size, PROT_READ | PROT_WRITE , MAP_SHARED, fd, 0)) < 0 ){
    perror("mmap failed");
    exit(-4);
  }
  foreach_phdr(map_start,function,0);
	 
	 /* now, we unmap the file */
	 munmap(map_start, fd_stat.st_size);
  
  return 0;
}
