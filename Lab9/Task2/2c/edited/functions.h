#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <elf.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <sys/mman.h>
#include <fcntl.h> /*open*/





/*prints tests,strings and addresses.
 * to print int: testPrint(i,0,0,0);
 * to print string: testPrint(0,string,0,0);
 * to print address: testPrint(0,0,address,1);
 */
void testPrint(int printIndex,char * string, void * address,int addressFlag){
  if(printIndex != 0)
    printf("TEST %d\n",printIndex);
  if(string != 0)
    printf("%s\n",string);
  if(addressFlag == 1)
    printf("%p\n",address);
}

void checkArgs(int argc){
  if(argc !=2){
    printf("Error, Wrong number of arguments\n");
    exit(1);
  }
}

/*checks if ELF format.*/
int checkELF(int i,Elf32_Ehdr * header){
  
  if( 'E' !=header->e_ident[EI_MAG1] || 'L' !=header->e_ident[EI_MAG2] || 'F' !=header->e_ident[EI_MAG3] || ELFCLASS32 !=header->e_ident[EI_CLASS])
    return 0;
  printf("\nFile format is : %c%c%c32\n",header->e_ident[EI_MAG1],header->e_ident[EI_MAG2],header->e_ident[EI_MAG3]);
  return 1;
}

