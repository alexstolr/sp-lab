#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <elf.h>
#include "./functions.h"
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <sys/mman.h>
#include <fcntl.h> /*open*/


/*MUST BE FIXXED MAPPING FLAGS ALWAYS!!*/


void load_phdr(Elf32_Phdr *phdr, int fd){
  void * map,* vaddr = (void *)(phdr->p_vaddr & 0xfffff000);
  off_t offset = phdr->p_offset & 0xfffff000;
  size_t padding;
  if((phdr->p_type) == PT_LOAD){
    padding = phdr->p_vaddr & 0xfff;
    int protExec = 0,protRead = 0,protWrite = 0,count = 0;
    if(phdr->p_flags == PF_X){
      protExec = 1;
      count++;
    }
    if(phdr->p_flags == PF_W){
      protWrite = 1;
      count++;
    }
    if(phdr->p_flags == PF_W+PF_X){
      protWrite = 1;
      protExec = 1;
      count = count + 2;
    }
    if(phdr->p_flags == PF_R){
      protRead = 1;
      count++;
    }
    if(phdr->p_flags == PF_R+PF_X){
      protRead = 1;
      protExec = 1;
      count = count + 2;
    }
    if(phdr->p_flags == PF_R+PF_W){
      protRead = 1;
      protWrite = 1;
      count = count + 2;
    }
    if(phdr->p_flags == PF_R+PF_W+PF_X){
      protRead = 1;
      protWrite = 1;
      protExec = 1;
      count = count + 3;
    }
    if(count == 0)
      printf("%s","PROT_NONE");
    if(count == 1){
      if(protRead==1){
	if((map = mmap(vaddr, phdr->p_memsz + padding, PROT_READ, MAP_PRIVATE|MAP_FIXED, fd, offset)) < 0){
	  perror("mmap failed");
	  exit(-4);
	}
      }
      if(protWrite==1){
	if((map = mmap(vaddr, phdr->p_memsz + padding, PROT_WRITE, MAP_PRIVATE|MAP_FIXED, fd, offset)) < 0){
	  perror("mmap failed");
	  exit(-4);
	}
      }
      if(protExec==1){
	if((map = mmap(vaddr, phdr->p_memsz + padding, PROT_EXEC, MAP_PRIVATE|MAP_FIXED, fd, offset)) < 0){
	  perror("mmap failed");
	  exit(-4);
	}
      }
    }
    else if(count == 2){
      if(protRead==1 && protWrite==1){
	if((map = mmap(vaddr, phdr->p_memsz + padding, PROT_READ | PROT_WRITE, MAP_PRIVATE|MAP_FIXED, fd, offset)) < 0){
	  perror("mmap failed");
	  exit(-4);
	}
      }
      if(protExec==1 && protWrite==1){
	if((map = mmap(vaddr, phdr->p_memsz + padding, PROT_EXEC | PROT_WRITE, MAP_PRIVATE|MAP_FIXED, fd, offset)) < 0){
	  perror("mmap failed");
	  exit(-4);
	}
      }
      if(protExec==1 && protRead==1){
	if((map = mmap(vaddr, phdr->p_memsz + padding, PROT_EXEC | PROT_READ, MAP_PRIVATE|MAP_FIXED, fd, offset)) < 0){
	  perror("mmap failed");
	  exit(-4);
	}
      }
    }
    else{
      if((map = mmap(vaddr, phdr->p_memsz + padding, PROT_READ | PROT_WRITE | PROT_EXEC, MAP_PRIVATE|MAP_FIXED, fd, offset)) < 0){
	  perror("mmap failed");
	  exit(-4);
	}
    }
  }
}

void function(Elf32_Phdr * progHeader,int i){
  /*printf("Program header number %d at address %#08x\n",i,(progHeader[i]).p_paddr );*/
}

void checkFlags(Elf32_Phdr * progHeader){
  if(progHeader->p_flags == PF_X){
    printf(" %-05s","E");
  }
  if(progHeader->p_flags == PF_W){
    printf(" %-05s","W");
  }
  if(progHeader->p_flags == PF_W+PF_X){
    printf(" %-05s","W E");
  }
  if(progHeader->p_flags == PF_R){
    printf(" %-05s","R");
  }
  if(progHeader->p_flags == PF_R+PF_X){
    printf(" %-05s","R E");
  }
  if(progHeader->p_flags == PF_R+PF_W){
    printf(" %-05s","RW");
  }
  if(progHeader->p_flags == PF_R+PF_W+PF_X){
    printf(" %-05s","R W E");
  }
}

void printfProtFlag1a(Elf32_Phdr * progHeader){
  if((progHeader->p_type) == PT_LOAD){
    printf("\t%s","protection flags: ");
    int protExec = 0,protRead = 0,protWrite = 0,count = 0;
    if(progHeader->p_flags == PF_X){
      protExec = 1;
      count++;
    }
    if(progHeader->p_flags == PF_W){
      protWrite = 1;
      count++;
    }
    if(progHeader->p_flags == PF_W+PF_X){
      protWrite = 1;
      protExec = 1;
      count = count + 2;
    }
    if(progHeader->p_flags == PF_R){
      protRead = 1;
      count++;
    }
    if(progHeader->p_flags == PF_R+PF_X){
      protRead = 1;
      protExec = 1;
      count = count + 2;
    }
    if(progHeader->p_flags == PF_R+PF_W){
      protRead = 1;
      protWrite = 1;
      count = count + 2;
    }
    if(progHeader->p_flags == PF_R+PF_W+PF_X){
      protRead = 1;
      protWrite = 1;
      protExec = 1;
      count = count + 3;
    }
    if(count == 0)
      printf("%s","PROT_NONE");
    if(count == 1){
      if(protRead==1)
	printf("%s","PROT_READ");
      if(protWrite==1)
	printf("%s","PROT_WRITE");
      if(protExec==1)
	printf("%s","PROT_EXEC");
    }
    else if(count == 2){
      if(protRead==1 && protWrite==1)
	printf("%s","PROT_READ | PROT_WRITE");
      if(protExec==1 && protWrite==1)
	printf("%s","PROT_EXEC | PROT_WRITE");
      if(protExec==1 && protRead==1)
	printf("%s","PROT_EXEC | PROT_READ");
    }
    else
      printf("%s","PROT_READ | PROT_WRITE | PROT_EXEC");
  }
}

char * checkType(Elf32_Phdr * progHeader){
  if((progHeader->p_type) == PT_NULL){
    return "";
  }
  if((progHeader->p_type) == PT_LOAD){
    return "LOAD";
  }
  if((progHeader->p_type) == PT_DYNAMIC){
    return "DYNAMIC";
  }
  if((progHeader->p_type) == PT_INTERP){
    return "INTERP";
  }
  if((progHeader->p_type) == PT_NOTE){
    return "NOTE";
  }
  if((progHeader->p_type) == PT_SHLIB){
    return "SHLIB";
  }
  if((progHeader->p_type) == PT_PHDR){
    return "PHDR";
  }
  if((progHeader->p_type) == PT_LOPROC){
    return "LOPROC";
  }
  if((progHeader->p_type) == PT_HIPROC){
    return "HIPROC";
  }
  return "";
}

void task1a(Elf32_Phdr * progHeader){
  if(strcmp(checkType(progHeader),"")){
    printf("  %-10s%#08x %#010x %#010x %#07x %#07x",checkType(progHeader),progHeader->p_offset,progHeader->p_vaddr,progHeader->p_paddr,progHeader->p_filesz,progHeader->p_memsz);
    checkFlags(progHeader);
    printf("%#x",progHeader->p_align);
    printfProtFlag1a(progHeader);
    printf("\n");
  }
}

int foreach_phdr(void *map_start, void (*func)(Elf32_Phdr *,int), int arg){
  Elf32_Ehdr * header = (Elf32_Ehdr *) map_start;
  if((checkELF(4,header)) == 0){
    printf("Error, the file you have chosen is not of 'ELF32' format!\n");
    exit(1);
  }
  int num_of_progHeaders = header->e_phnum;
  Elf32_Phdr * progHeader = (Elf32_Phdr *)(map_start+header->e_phoff);
  int i;
  printf("Program Headers:\n  Type      Offset   VirtAddr   PhysAddr   FileSiz MemSiz  Flg Align\n");
  for(i = 0; i< num_of_progHeaders ; i++){
    task1a(progHeader);
    /*printf("-15%s\n",(progHeader[i].p_type).PT_PHDR);*/
    func(progHeader,arg);
    progHeader++;
  }
  return 0;
}


int main(int argc, char ** argv){
  checkArgs(argc);
  int fd;
  void * map_start;
  struct stat fd_stat;
  if( (fd = open(argv[1], O_RDWR)) < 0 ){
    perror("error in open");
    exit(-1);
  }
  if( fstat(fd, &fd_stat) != 0 ){
    perror("stat failed");
    exit(-1);
  }
  if( (map_start = mmap(0, fd_stat.st_size, PROT_READ | PROT_WRITE , MAP_SHARED, fd, 0)) < 0 ){
    perror("mmap failed");
    exit(-4);
  }
  foreach_phdr(map_start,load_phdr,fd);
  
  /* now, we unmap the file */
  munmap(map_start, fd_stat.st_size);
  
  return 0;
}
