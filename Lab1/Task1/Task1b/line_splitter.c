#include <stdlib.h>
#include <stdio.h>
#include <string.h>

/*split the input into lines after every occurrence of a digit and/or a punctuation mark.
 *depanding on valuse of flags p,d.
 */
void pdSplit(char chr,int p){
  if(chr >= 48 && chr <= 57){
    fputc(chr,stdout);
    fputc('\n',stdout);
  }
  else{
    if(p==0)
      fputc(chr,stdout);
    else
      pSplit(chr);
  }
}

/*split the input into lines after every occurrence of a punctuation mark*/
void pSplit(char chr){
  switch(chr){
    case ';':
    case ':':
    case ',':
    case '.':
      fputc(chr,stdout);
      chr = '\n';
    default:
      fputc(chr,stdout);  
  }
}

int main(int argc, char **argv){
  int p,d,i;
  char chr;
  p=0;
  d=0;
  
  /*init flags.*/
  for(i=1; i<argc; i++){
    if(strcmp(argv[i],"-p")==0)
      p=1;
    else if(strcmp(argv[i],"-d")==0)
      d=1;
    else{
      printf("%s\n", "Error, wrong input");
      return 1;
    }
  }
  
  if(d==1 && p==0){
    while((chr = fgetc(stdin)) != EOF)
      pdSplit(chr,p);
  }
  if(d==1 && p==1){
    while((chr = fgetc(stdin)) != EOF)
      pdSplit(chr,p);
  }
  else{
    while((chr = fgetc(stdin)) != EOF)
      pSplit(chr);
  }
  return 0;
}





