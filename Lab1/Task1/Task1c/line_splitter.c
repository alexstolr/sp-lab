#include <stdlib.h>
#include <stdio.h>
#include <string.h>

/*split the input into lines after every occurrence of a punctuation mark*/
void pSplit(char chr,FILE * outtype){
  switch(chr){
    case ';':
    case ':':
    case ',':
    case '.':
      fputc(chr,outtype);
      chr = '\n';
    default:
      fputc(chr,outtype);  
  }
}

/*split the input into lines after every occurrence of a digit and/or a punctuation mark.
 *depanding on valuse of flags p,d.
 */
void pdSplit(char chr, int p, FILE * outtype){
  if(chr >= 48 && chr <= 57){
    fputc(chr,outtype);
    fputc('\n',outtype);
  }
  else{
    if(p==0)
      fputc(chr,outtype);
    else
       pSplit(chr,outtype);
  }
}

/*write to output.*/
void writeOut(FILE * outtype,int d, int p){
  char chr;
  if(d==1 && p==0){
    while((chr = fgetc(stdin)) != EOF)
      pdSplit(chr,p,outtype);
  }
  if(d==1 && p==1){
    while((chr = fgetc(stdin)) != EOF)
      pdSplit(chr,p,outtype);
  }
  else{
    while((chr = fgetc(stdin)) != EOF){
      pSplit(chr,outtype);
    }
  }
}

void chooseOutType(FILE * fileToWrite,int d, int p, int o){
  /*write to stdout.*/
  if(o==0){
    writeOut(stdout,d,p);
  }
  /*write to file.*/
  if(o==1){
    writeOut(fileToWrite,d,p);
  }
}

int main(int argc, char **argv){
  int p=0,d=0,o=0,i;
  FILE * fileToWrite;
  char * outFileName;
  for(i=1; i<argc; i++){
    if(strcmp(argv[i],"-p")==0)
      p=1;
    else if(strcmp(argv[i],"-d")==0)
      d=1;
    else if(strcmp(argv[i],"-o")==0){
      o=1;
      outFileName = argv[++i];
      fileToWrite = fopen(outFileName,"w");
    }
    else{
      printf("%s\n","Error! wrong input");
      return 1;
    }
  }
  chooseOutType(fileToWrite,d,p,o);
  fclose(fileToWrite); 
  return 0;
}





