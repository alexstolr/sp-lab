#include <stdio.h>

int main(){
  char chr;
  while((chr = fgetc(stdin)) != EOF){
    switch(chr){
      case ';':
      case ':':
      case ',':
      case '.':
	fputc(chr,stdout);
	chr = '\n';
      default:
	  fputc(chr,stdout);  
    }
  }
  return 0;
}