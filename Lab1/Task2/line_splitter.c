#include <stdlib.h>
#include <stdio.h>
#include <string.h>


/*split by panctuation marks.*/
void pSplit(char chr,FILE * outtype){
  switch(chr){
    case ';':
    case ':':
    case ',':
    case '.':
      fputc(chr,outtype);
      chr = '\n';
    default:
      fputc(chr,outtype);  
  }
}

/*split the input into lines after every occurrence of a digit and/or a punctuation mark.
 *depanding on valuse of flags p,d.
 */
void pdSplit(char chr, int p, FILE * outtype){
  if(chr >= 48 && chr <= 57){
    fputc(chr,outtype);
    fputc('\n',outtype);
  }
  else{
    if(p==0)
      fputc(chr,outtype);
    else
       pSplit(chr,outtype);
  }
}

/*write to output.*/
void writeOut(FILE * outtype,int d, int p, FILE * inType){
  char chr;
  if(d==1 && p==0){
    while((chr = fgetc(inType)) != EOF)
      pdSplit(chr,p,outtype);
  }
  if(d==1 && p==1){
    while((chr = fgetc(inType)) != EOF)
      pdSplit(chr,p,outtype);
  }
  else{
    while((chr = fgetc(inType)) != EOF){
      pSplit(chr,outtype);
    }
  }
}

void chooseIOType(FILE * fileToWrite,int d, int p,int o, FILE * inType){
  if(o==0){	/*writes output to screen.*/
    writeOut(stdout,d,p, inType);
  }
  if(o==1){	/*writes output to file.*/
    writeOut(fileToWrite,d,p, inType);
  }
}


int main(int argc, char **argv){
  int p=0,d=0,o=0,i;
  FILE * fileToWrite;
  FILE * filesToRead[10];
  int numOfInFIle = 0; /*flag for max 10 input files*/
  char * outFileName;
  /*initiate flags from args*/
  for(i=1; i<argc; i++){
    if(strcmp(argv[i],"-p")==0)
      p=1;
    else if(strcmp(argv[i],"-d")==0)
      d=1;
    else if(strcmp(argv[i],"-o")==0){
      o=1;
      outFileName = argv[++i];
      fileToWrite = fopen(outFileName,"w");
    }
    else{
      filesToRead[numOfInFIle] = fopen(argv[i], "r");
      if(numOfInFIle>=10){
	printf("%s\n","Error! up to 10 input files allowed");
	return 1;
      }
      numOfInFIle++; 
    }
  }
  /*if 0 in files - read from stdin.*/
  if(numOfInFIle == 0){
    chooseIOType(fileToWrite,d,p,o,stdin);
  }
  else{	/*read from files*/
    for(i=0 ; i < numOfInFIle ; i++){
      chooseIOType(fileToWrite,d,p,o,filesToRead[i]);
    }
  }
  fclose(fileToWrite); 
  return 0;
}





