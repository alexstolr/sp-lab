#include <stdio.h>
#include <stdlib.h>

void parseVir(FILE * binary);
void printVir(int length, char * signature, char * name);
int virNam(FILE * binary,char * name);
int virSig(FILE * binary,char * signature, int sigLength);
int sigLength(FILE * binary);

/*returns length of the virus signature*/
int sigLength(FILE * binary){
  char * sizBuffer = malloc(1);
  int bytesRead = fread(sizBuffer,1,1,binary);
  int length;
  if( bytesRead == 0 || (((int)sizBuffer[0]) < 0) ){ 
    free(sizBuffer);
    return -1;
  }
  else{
    length =  ((int)sizBuffer[0]); 
    free(sizBuffer); 
  }
  return length;
  
}

/* reads virus's signature into signature*/
int virSig(FILE * binary,char * signature, int sigLength){
  if( (fread(signature,1,sigLength,binary)) <= 0 ){
    printf("Error reading binary\n");
    return -1;
  }
  return 0;
}


/* reads virus's name into name*/
int virNam(FILE * binary,char * name){
  int i = 0;
  while(1){
    fread((name+i),1,1,binary);
    if (name[i] == EOF){
      return 1;/*in this case should finish the printing.*/
    }
    if (name[i] == 00){
      break;
    }
    i++;
  }
  return 0;
}

/*parses viruses to name signature and length*/
void parseVir(FILE * binary){
  int length = 0;
  while(1){
    if((length = sigLength(binary)) == -1)
      exit(1);
    char * signature = malloc(length);
    if((virSig(binary,signature, length)) == -1)
      exit(1);
    char * name = malloc(100);
    if((virNam(binary,name)) == 1)
      exit(0);
    printVir(length,signature,name);
  }
}

/*prints virus details*/
void printVir(int length, char * signature, char * name){
  printf("Virus name: %s\n", name);
  free(name);
  printf("Virus size: %d\n", length);
  printf("Virus signature: \n");
  int i;
  for(i = 0; i<length;i++){
    printf("%02X",  (int)(*(unsigned char*)(&signature[i])) );
    printf(" ");
  }
  printf("\n\n");
  free(signature);
}


int main(int argc, char** argv){
  FILE * binary = fopen(argv[1], "r");
  if (!binary){
    printf("Unable to open file!\n");
    return 1;
  }
  parseVir(binary);
  fclose(binary);
  return 0;
}	





