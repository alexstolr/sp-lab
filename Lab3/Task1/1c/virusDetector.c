#include <stdio.h>
#include <stdlib.h>
#include<string.h>

void parseVir(FILE * binary);
void printVir(int length, char * signature, char * name);
int virNam(FILE * binary,char * name);
int virSig(FILE * binary,char * signature, int sigLength);
int sigLength(FILE * binary);

typedef struct virus virus;

struct virus {
  int length;
  char *signature;
  char *name;
  virus *next;
};

virus  crt_virus(char * name,char * signature , int length){
  virus vir;
  vir.name = name;
  vir.signature = signature;
  vir.length = length;
  vir.next = NULL;
  return vir;
}

/* Add a new link with the given data to the end
 * of the list and return a pointer to the list (i.e., the first link in the list).
 * If list is null - create a new node and return a pointer to this node. */
virus *list_append(virus *virus_list, virus * data){
  virus * tmp;
  if(virus_list==NULL){
    virus_list = data; 
  }
  else{
    tmp = virus_list;
    while(tmp->next != NULL)
      tmp = tmp->next;
    tmp->next = data;
  }
  return virus_list;
}

/* Print the data of every link in list. Each data is followed by a newline character. */
void list_print(virus * virus_list){
  virus * tmp = virus_list;
  while(tmp->next != NULL){
    printf("Virus name: %s\n", tmp->name); 
    printf("Virus size: %d\n",tmp->length);
    printf("Virus signature: \n");	
    int i;
    for(i = 0; i<tmp->length;i++){
      printf("%02X ", tmp->signature[i] & 0xFF);
    }
    printf("\n\n");
    tmp = tmp->next;
  }
}

/* Free the memory allocated by the list. */
void list_free(virus *virus_list){
  virus * tmp1 = virus_list;
  virus * tmp2 = virus_list;
  while(tmp1->next != NULL){
    tmp2 = tmp1->next;
    free(tmp1->name);
    free(tmp1->signature);
    free(tmp1);
    tmp1 = tmp2;
  }
  free(tmp1->name);
  free(tmp1->signature);
  free(tmp1);
}


/*returns length of the virus signature*/
int sigLength(FILE * binary){
  char * sizBuffer = malloc(1);
  int bytesRead = fread(sizBuffer,1,1,binary);
  int length;
  if( bytesRead == 0 || (((int)sizBuffer[0]) < 0) ){ 
    free(sizBuffer);
    return -1;
  }
  else{
    length =  ((int)sizBuffer[0]); 
    free(sizBuffer); 
  }
  return length;
  
}

/* reads virus's signature into signature*/
int virSig(FILE * binary,char * signature, int sigLength){
  if( (fread(signature,1,sigLength,binary)) <= 0 ){
    return 0;
  }
  return 0;
}


/* reads virus's name into name*/
int virNam(FILE * binary,char * name){
  int i = 0;
  while(1){
    fread((name+i),1,1,binary);
    if (name[i] == EOF){
      return 1;/*in this case should finish the printing.*/
    }
    if (name[i] == 00){
      break;
    }
    i++;
  }
  return 0;
}

/*parses viruses to name signature and length*/
void parseVir(FILE * binary){
  int length = 0;
  while(1){
    if((length = sigLength(binary)) == -1)
      exit(1);
    char * signature = malloc(length);
    if((virSig(binary,signature, length)) == -1)
      exit(1);
    char * name = malloc(100);
    if((virNam(binary,name)) == 1)
      exit(0);
    printVir(length,signature,name);
  }
}

/*prints virus details*/
void printVir(int length, char * signature, char * name){
  printf("Virus name: %s\n", name);
  free(name);
  printf("Virus size: %d\n", length);
  printf("Virus signature: \n");
  int i;
  for(i = 0; i<length;i++){
    printf("%02X",  (int)(*(unsigned char*)(&signature[i])) );
    printf(" ");
  }
  printf("\n\n");
  free(signature);
}

virus * crtList(FILE * binary,virus * virus_list){
  int length = 0,flag = 0;
  while(flag ==0){
    if((length = sigLength(binary)) == -1){
      flag = 1;
    }
    char * signature = malloc(length);
    if((virSig(binary,signature, length)) == -1)
      exit(1);
    char * name = malloc(100);
    if((virNam(binary,name)) == 1)
      return virus_list;
    virus * vir=(virus *) malloc(sizeof(virus));
    *vir = crt_virus(name, signature, length);
    virus_list = list_append(virus_list, vir);
  }
  return virus_list;
}

void readSuspected(FILE * suspected,char * suspectedBuffer){
  size_t itemsRead = 0;
  if (suspected != NULL) {
    itemsRead = fread(suspectedBuffer, sizeof(char), 10000, suspected);
    if (itemsRead == 0) {
        fputs("Error reading file", stderr);
    }
    else {
        suspectedBuffer[++itemsRead] = '\0';
    }
    fclose(suspected);
  }
}

void detect_virus(char *suspectedBuffer, virus *virus_list, unsigned int sz){
  int i = 0;
  virus * ptr = virus_list;
  virus * tmp = ptr; 
  int virusFound = 0;
  while(i<sz){
    while(tmp != NULL){
      
      if(memcmp(tmp->signature, suspectedBuffer+i, tmp->length)==0){
	virusFound = 1;
	printf("WARNNING! VIRUS FOUND!\n");
	printf("Starting byte location in the suspected file: [decimal]%d,[hexadecimal]%02X\n",i,i);
	printf("Virus name: %s\n",tmp->name);
	printf("Virus size: %d\n\n", tmp->length);
	break;
      }
      else{
	tmp= tmp->next;
      }
      if(tmp->next == NULL){
	break; 
      }
    }
    tmp =ptr;
    i++; 
  }
  if(virusFound == 0)
    printf("there are no viruses! :)\n");
}

/*
void detect_virus(char *suspectedBuffer, virus *virus_list, unsigned int sz){
  int i = 0;
  virus * ptr = virus_list;
  virus * tmp = ptr;
  while(i<sz){
   while(tmp != NULL){
     
     if(memcmp(tmp->signature, suspectedBuffer+i, tmp->length)==0){
       printf("WARNING! VIRUS [%s] FOUND \n",tmp->name);
       break;
     }
     else{
      tmp= tmp->next;
     }
     if(tmp->next == NULL){
      break; 
     }
   }
   tmp =ptr;
   i++; 
  }
}*/


int main(int argc, char** argv){
  FILE * signatures = fopen(argv[1], "r");
  FILE * suspected = fopen(argv[2], "r");
  if (!signatures){
    puts("Unable to open signatures file!");
    return 1;
  }
  if (!suspected){
    puts("Unable to open suspected file!");
    return 1;
  }
  virus * virus_list = NULL;
  virus_list = crtList(signatures,virus_list);
  
  /*puts("printing list..\n");
  list_print(virus_list);
  puts("finished printing list..");
  */
  
  char suspectedBuffer[10000];
  readSuspected(suspected,suspectedBuffer);
  detect_virus(suspectedBuffer,virus_list,10000);
  return 0;
}	





