#include <stdlib.h>
#include <stdio.h>

void PrintHex(int * buffer, int length){
  printf("%02X", buffer[length]);
  printf(" ");
}


int main(int argc, char** argv){
  /*printf("number of args is: %d\n", argc);*/
  FILE* binary = fopen(argv[1],"r");
  int* content= malloc(4*sizeof(int));
  int flag = 0;
  while(flag != 1){
    size_t len = fread(content, 1, 1, binary);
    if(len == 0)
      flag = 1;
    else{
      PrintHex(content,0);
    }
  }
  printf("\n");
  free(content);
  fclose(binary);
  return 0;
}