#include <stdlib.h>
#include <stdio.h>
#include <errno.h>
#include <fcntl.h>
#include <unistd.h>
#include <string.h>

#define O_RDONLY         00
#define O_WRONLY         01
#define O_RDWR           02

typedef struct funcMenu {
  char * name;
  void (* fun)(char *,int);
} funcMenu;


/*prints tests,strings and addresses.
 * to print int: testPrint(i,0,0,0);
 * to print string: testPrint(0,string,0,0);
 * to print address: testPrint(0,0,address,1);
 */
void testPrint(int printIndex,char * string, void * address,int addressFlag){
  if(printIndex != 0)
    printf("TEST %d\n",printIndex);
  if(string != 0)
    printf("%s\n",string);
  if(addressFlag == 1)
    printf("%p\n",address);
}

void function1(char * filename, int size){
  testPrint(1,0,0,0);
}

void function2(char * filename, int size){
  testPrint(2,0,0,0);
}

void quitMenu(char * filename, int size){
  printf("option 3 chosen..\nquiting program..\n");
  exit(0);
}

void printMenu(char * filename,funcMenu menu[]){
  char * menuText = "choose action:"; 
  printf("File: %s,%s\n",filename,menuText);
  int i;
  for(i=0; i<3 ; i++){
    printf("%s\n",menu[i].name); 
  }
}

int main(int argc, char ** argv){
  if(argc <= 0 || argc > 3){
    printf("Error, wrong arguments\n");
    exit(1);
  }
  char * filename = argv[1];
  int size;
  if(argc == 3){
     size = atoi(argv[2]);
     if(size != 1 && size != 2 && size != 4){
      printf("Error, wrong argument for size\n");
      exit(1);
     }
  }
  int fd;
  if((fd = open(filename , O_RDWR)) < 0){/*TODO close!*/
      printf( "Error opening file: %s\n", strerror( errno ) );
      exit(1);
    }
  int menuSize = 3;
  char buf[menuSize];
  int funcChosen;
  funcMenu menu[3] = {
    {"1-Mem Display",&function1},
    {"2-File Display",&function2},
    {"3-Quit",&quitMenu}
  };
  printMenu(filename,menu);
  if(fgets(buf,menuSize + 1,stdin) == 0){/*'+1' for case menu has only 1 option.*/
    printf("Error, could not read input from user.");
    exit(1);
  }
  funcChosen = atoi(buf);
  if(funcChosen < 0 || funcChosen > menuSize){
      printf("ERROR, chosen function out of bound\n");
      exit(1);
    }
  (menu[funcChosen-1]).fun(filename,1);
  return 0;
}
