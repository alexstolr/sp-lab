#include <stdlib.h>
#include <stdio.h>
#include <errno.h>
#include <fcntl.h>
#include <unistd.h>
#include <string.h>

#define O_RDONLY         00
#define O_WRONLY         01
#define O_RDWR           02

/*answer:
 * run: 'readelf -s hexeditplus' and find the word "text" the address is 080484c0.
 * type 'hexedit hexeditplus' and find the address '4c0' or near it.
 * now if run the program with this address it will be the same as when run in 'hexedit hexeditplus'
 */

typedef struct funcMenu {
  char * name;
  void (* fun)(char *,int);
} funcMenu;


/*prints tests,strings and addresses.
 * to print int: testPrint(i,0,0,0);
 * to print string: testPrint(0,string,0,0);
 * to print address: testPrint(0,0,address,1);
 */
void testPrint(int printIndex,char * string, void * address,int addressFlag){
  if(printIndex != 0)
    printf("TEST %d\n",printIndex);
  if(string != 0)
    printf("%s\n",string);
  if(addressFlag == 1)
    printf("%p\n",address);
}

void memDisp(char * filename, int size){
  printf("Please enter <address> <length> \n");
  unsigned char * address;
  int length;
  char buf[64];
  fgets(buf,64,stdin);
  sscanf(buf,"%p %d\n", &address, &length);
   int i=0;
  /*&0XFF*/
  if(size==2){
    unsigned short * pointer = (unsigned  short *)address;
    for(i=0; i<length; i++){
      printf("%04X" , *(pointer+i));
      printf(" ");
    }
  }
  else if(size==4){
    unsigned int * pointer = (unsigned  int *)address;
    for(i=0; i<length; i++){
      printf("%08X" , *(pointer+i));
      printf(" ");
    }
  }
  else{
    for(i=0; i<length; i++){
      printf("%02X" , *(address+i));
      printf(" ");
    }
  }
  printf("\n");
}

/*void memDisp(char * filename, int size){
  printf("Please enter <address> <length> \n");
  unsigned char * address;
  int length;
  char buf[64];
  fgets(buf,64,stdin);
  sscanf(buf,"%p %d\n", &address, &length);
  int i;
  int j;
  for(i=0;i<length;i++){
    for(j=0 ; j<size;j++){
      printf("%02X",address[i*size+j]);
    }
    printf(" ");
  }
  printf("\n");
}*/


/*void fileDisp(char * filename, int size){
  printf("Please enter <location> <length> \n");
  int length;
  unsigned int location;
  char buf[64];
  fgets(buf,64, stdin);
  sscanf(buf,"%x %d\n", &location, &length);
  FILE * file;
  if((file = fopen(filename , "r")) == 0){
    printf( "Error opening file: %s\n", strerror( errno ) );
    exit(1);
  }
  fseek(file,location,SEEK_SET);
  int i;
  int j;
  unsigned char buf2[1000];
  fread(&buf2, length*size, 1, file);
  for(i=0;i<length;i++){
    for(j=0 ; j<size;j++){
      printf("%02X",buf2[i*size+j]);
    }
    printf(" ");
  }
  printf("\n");
   fclose(file);
}*/

void fileDisp(char * filename, int size){
  printf("Please enter <location> <length> \n");
  int length;
  char * location;
  char buf[1024];
  fgets(buf,1024, stdin);
  sscanf(buf,"%p %d\n", &location, &length);
  FILE * file;
  if((file = fopen(filename , "r")) == 0){
    printf( "Error opening file: %s\n", strerror( errno ) );
    exit(1);
  }
  fseek(file,(long)location,SEEK_SET);
  char buf2[1024];
  fgets(buf2, size*length , file);
  int i;
  if(size==2){
    unsigned short * pointer = (unsigned  short *)buf2;
    for(i=0; i<length; i++){
      printf("%04X" , *(pointer+i));
      printf(" ");
    }
  }
  else if(size==4){
    unsigned int * pointer = (unsigned  int *)buf2;
    for(i=0; i<length; i++){
      printf("%08X" , *(pointer+i));
      printf(" ");
    }
  }
  else{
    for(i=0; i<length; i++){
      printf("%02X" , *(buf2+i));
      printf(" ");
    }
  }
  printf("\n");
  fclose(file);
}

void fileMod(char * filename, int size){
  printf("Please enter <location> and <val> (2 bytes) \n");
  unsigned int val,location;
  char buf[64];
  fgets(buf,64, stdin);
  sscanf(buf, "%x %x",&location, &val);
  FILE * file= fopen(filename,"rb+");
  fseek(file,location,SEEK_SET);
  /*fwrite(&val,sizeof(val),1,file);*/
  fwrite(&val,size,1,file);
  /*fwrite(&val, size , (sizeof(val)/size),file);*/
  printf("done writing to file\n");
  fclose(file);
}

void cpFile(char * filename, int size){
  printf("Please enter <source-file> <s-location> <t-location> <length> \n");
  char * buf=malloc(256);
  fgets(buf,256, stdin);
  char source[64];
  int sourceLoc,destLoc,length;
  sscanf(buf, "%s %X %X %d",source,&sourceLoc,&destLoc, &length);
  free(buf);
  FILE * sFile = fopen(source,"r");
  fseek(sFile,0,SEEK_END);/*set indicator at the end*/
  int sourceSize = ftell(sFile);
  fseek(sFile,0,SEEK_SET);
  FILE * destFile= fopen(filename,"r+");
  fseek(destFile,0,SEEK_END);
  int destSize = ftell(destFile);
  fseek(destFile,0,SEEK_SET);
  if( ((sourceLoc+ size*length) > sourceSize) || (destLoc > destSize) ){
    printf("Error, The size limit was exceeded\n");
    exit(1);
  }
  buf = malloc(2048);
  fseek(sFile, sourceLoc, SEEK_SET);
  fseek(destFile, destLoc, SEEK_SET);
  fread(&buf, length * size, 1, sFile);
  fseek(destFile,destLoc,SEEK_SET);
  fwrite(&buf, length * size, 1, destFile);
  fclose(sFile);
  fclose(destFile);
}



void quit(char * filename, int size){
  printf("exiting..\n");
  exit(0);
}

void printMenu(char * filename,funcMenu menu[],int menuSize){
  char * menuText = "choose action:"; 
  printf("File: %s,%s\n",filename,menuText);
  int i;
  for(i=0; i<menuSize ; i++){
    printf("%s\n",menu[i].name); 
  }
}

int main(int argc, char ** argv){
  if(argc <= 0 || argc > 3){
    printf("Error, wrong arguments\n");
    exit(1);
  }
  char * filename = argv[1];
  int unitSize = 1;
  if(argc == 3){
    unitSize = atoi(argv[2]);
    if(unitSize != 1 && unitSize != 2 && unitSize != 4){
      printf("Error, wrong argument for size\n");
      exit(1);
    }
  }
  /*
   *  int fd;
   *  if((fd = open(filename , O_RDWR)) < 0){
   *    printf( "Error opening file: %s\n", strerror( errno ) );
   *    exit(1);
}
*/
  int menuSize = 5;
  char buf[menuSize];
  int funcChosen;
  funcMenu menu[5] = {
    {"1-Mem Display",&memDisp},
    {"2-File Display",&fileDisp},
    {"3-File Modify",&fileMod},
    {"4-Copy from File",&cpFile},
    {"5-Quit",&quit}
  };
  printMenu(filename,menu,menuSize);
  if(fgets(buf,menuSize + 1,stdin) == 0){/*'+1' for case menu has only 1 option.*/
    printf("Error, could not read input from user.");
    exit(1);
  }
  funcChosen = atoi(buf);
  if(funcChosen < 0 || funcChosen > menuSize){
    printf("ERROR, chosen function out of bound\n");
    exit(1);
  }
  (menu[funcChosen-1]).fun(filename,unitSize);
  return 0;
}

/*
 * check: have one dest abc and one source abc.
 * open hexedit abcdest and find addres 12f - it says there: '00 01 00 00 00 2f 6c 69 62'
 * now run: './hexeditplus abcdest'
 * 4
 * Please enter <source-file> <s-location> <t-location> <length>
 * abc 120 12f 4
 * check if it changed to '14 9f 04 08'
 * 
 */
