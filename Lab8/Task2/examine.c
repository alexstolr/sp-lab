#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <elf.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <sys/mman.h>
#include <fcntl.h> /*open*/


/*headers*/
void testPrint(int printIndex,char * string, void * address,int addressFlag);
void checkArgs(int argc);

/*headers*/



/*prints tests,strings and addresses.
 * to print int: testPrint(i,0,0,0);
 * to print string: testPrint(0,string,0,0);
 * to print address: testPrint(0,0,address,1);
 */
void testPrint(int printIndex,char * string, void * address,int addressFlag){
  if(printIndex != 0)
    printf("TEST %d\n",printIndex);
  if(string != 0)
    printf("%s\n",string);
  if(addressFlag == 1)
    printf("%p\n",address);
}

void checkArgs(int argc){
  if(argc !=2){
    printf("Error, Wrong number of arguments\n");
    exit(1);
  }
}

/*checks if ELF format.*/
int checkELF(int i,Elf32_Ehdr * header){
  
  if( 'E' !=header->e_ident[EI_MAG1] || 'L' !=header->e_ident[EI_MAG2] || 'F' !=header->e_ident[EI_MAG3] || ELFCLASS32 !=header->e_ident[EI_CLASS])
    return 0;
  printf("\nFile format : %c%c%c32\n",header->e_ident[EI_MAG1],header->e_ident[EI_MAG2],header->e_ident[EI_MAG3]);
  return 1;
}

void task0(Elf32_Ehdr * header,Elf32_Shdr * sectHeader,Elf32_Phdr * progTable,int num_of_section_headers,int num_of_program_headers){
  int i;
  printf("The data encoding scheme of the object file: %d",header->e_ident[EI_DATA]);
  if(1 == header->e_ident[EI_DATA])
    printf(" --> 2's complement, little endian\n");
  else printf("\n");
  printf("Entry point: %p\n",(void *)header->e_entry);
  printf("section header file offset :%d\n",header->e_shoff);
  printf("number of section header entries :%d\n",header->e_shnum);
  printf("\n");
  for(i = 0; i< num_of_section_headers ; i++){
    printf("size of section %d is: %d\n",i,sectHeader[i].sh_size);
  }
  printf("\n");
  printf("header table file offset :%d\n",header->e_phoff);
  printf("number of program header entries :%d\n",header->e_phnum);
  printf("\n");
  for(i = 0; i< num_of_program_headers ; i++){
    printf("size of program header entry %d is: %d\n",i,progTable[i].p_filesz);
  }
}

void task1(int num_of_section_headers,Elf32_Shdr * sectHeader,char * section_string_table){
  int i;
  printf("\n[Nr]   Name                     Addr             Off            Size   \n");
  for(i = 0; i< num_of_section_headers ; i++){
    printf("[%02d]  %-20s      %08x         %06x         %06x\n" , i,  section_string_table+sectHeader[i].sh_name , sectHeader[i].sh_addr , sectHeader[i].sh_offset , sectHeader[i].sh_size);
  } 
}



int main(int argc, char ** argv){
  checkArgs(argc);
  int fd;
  void *map_start; /* will point to the start of the memory mapped file */
  struct stat fd_stat; /* this is needed to  the size of the file */
  Elf32_Ehdr * header; /* this will point to the header structure */
  Elf32_Shdr * sectHeader;
  Elf32_Shdr  symSection;
  Elf32_Phdr * progTable;
  Elf32_Sym * symTable;
  int i , num_of_section_headers , num_of_program_headers , section_header_offset;
  char * section_string_table;
  char * symbol_string_table;
  int symSections[2];
  
  if( (fd = open(argv[1], O_RDWR)) < 0 ){
    perror("error in open");
    exit(-1);
  }
  if( fstat(fd, &fd_stat) != 0 ){
    perror("stat failed");
    exit(-1);
  }
  if( (map_start = mmap(0, fd_stat.st_size, PROT_READ | PROT_WRITE , MAP_SHARED, fd, 0)) < 0 ){
    perror("mmap failed");
    exit(-4);
  }
  header = (Elf32_Ehdr *) map_start;
  if((checkELF(4,header)) == 0){
    printf("Error, the file you have chosen is not of 'ELF32' format!\n");
    exit(1);
  }
  
  /*define*/
  sectHeader = (Elf32_Shdr *)(map_start + header->e_shoff);
  num_of_section_headers = header->e_shnum;
  num_of_program_headers = header->e_phnum;
  progTable = (Elf32_Phdr *)(map_start + header->e_phoff);
  section_string_table = (char *)(map_start + sectHeader[header->e_shstrndx].sh_offset);
  
  
  /*task0(header,sectHeader,progTable,num_of_section_headers,num_of_program_headers);
   *  task1(num_of_section_headers,sectHeader,section_string_table);*/
  
  int j;
  for (i=0; i < num_of_section_headers; i++){
    if(sectHeader[i].sh_type== SHT_SYMTAB || sectHeader[i].sh_type==SHT_DYNSYM){
      if(sectHeader[i].sh_type== SHT_SYMTAB)
	printf("%s%d%s","Symbol table '.dynsym' contains ", 8, " entries:");
      else
	printf("%s%d%s","Symbol table '.symtab' contains ", 67, " entries:");
      symTable = (Elf32_Sym *)((char *)(map_start + sectHeader[i].sh_offset));
      symbol_string_table = (char *)(map_start + sectHeader[sectHeader[i].sh_link].sh_offset);/* or: (sectHeader[i].sh_link).sh_offset*/
      printf("\n[index]\tvalue\t     section_index  section_name     \tsymbol_name   \n");
      for ( j=0; j<(sectHeader[i].sh_size/sectHeader[i].sh_entsize) ; j++ ){
	if(symTable[j].st_shndx != SHN_ABS){
	  printf("%02d\t%08x     ",j,symTable[j].st_value);
	  if( symTable[j].st_shndx == SHN_UNDEF)
	    printf("UND\t");
	  else if ( symTable[j].st_shndx == SHN_ABS)
	    printf("ABS\t");
	  else
	    printf("%02d \t",symTable[j].st_shndx);
	  printf("   %-20s%-20s     \n",section_string_table + sectHeader[symTable[j].st_shndx].sh_name,symbol_string_table + symTable[j].st_name);
	}
      }
    }
  }
  
  
  /* now, we unmap the file */
  munmap(map_start, fd_stat.st_size);
  
  return 0;
}
