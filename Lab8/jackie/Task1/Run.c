#include "utils.h"
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <elf.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <sys/mman.h>

int main (int argc, char * argv[])
{
  
  int fd;
  char * file_name;
  void * map_start;
  struct stat fd_stat; /* this is needed to  the size of the file */
  Elf32_Ehdr * header;
  Elf32_Shdr * section_table;
  char * section_string_table;
  
  if(argc == 1)
    puts("Usage: examine <file_name>");

  file_name = *(argv + 1);
  
  if((fd = open(file_name, 2,0777)) < 0)
  {
    perror(file_name);
    exit(1); 						/*failure*/
  }
  if( fstat(fd, &fd_stat) != 0 ) {
      perror("stat failed");
      exit(-1);
   }
  
  if ((map_start = mmap(NULL, fd_stat.st_size, PROT_READ|PROT_WRITE,MAP_SHARED, fd, 0)) == MAP_FAILED)
  {
    perror("mmap have failed!\n");
    exit(1);
  }
  header = (Elf32_Ehdr *)(map_start);
  section_table = (Elf32_Shdr *)(map_start + header -> e_shoff);
  
  section_string_table = (char *)(map_start + section_table[header->e_shstrndx].sh_offset);
  printf("%s\n", section_string_table+section_table[1].sh_name);
   
 /* print_sections(section_table, header -> e_shnum);*/
  
  munmap(header, sizeof(Elf32_Ehdr));
  
  exit(0);
  
}