#include "utils.h"

void print_header(Elf32_Ehdr * header)
{
  /*1*/
  printf("%c%c%c\n", header -> e_ident[EI_MAG1], header -> e_ident[EI_MAG2], header -> e_ident[EI_MAG3]);
  if(('E' != header -> e_ident[EI_MAG1]) | ('L' != header -> e_ident[EI_MAG2]) | ('F' != header -> e_ident[EI_MAG3]))
  {
    printf("Error: This is not a valid ELF file\n");
    exit(1);
  }
  
  /*2*/
  if(header -> e_ident[EI_DATA] == 0)
    printf("  Data:\t\t\t\t\tInvalid data encoding\n"); 
  else if(header -> e_ident[EI_DATA] == 1)
    printf("  Data:\t\t\t\t\t2's complement, little endian\n"); 
  else if(header -> e_ident[EI_DATA] == 2)
    printf("  Data:\t\t\t\t\t2's complement, big endian\n"); 
  else
    printf("  Data:\t\t\t\t\tCORRUPTED\n"); 
  
  /*3*/
  printf("  Entry point address:\t\t\t%p\n", (void *)header -> e_entry); 
  
  /*4*/
  printf("  Start of section headers:\t\t%d (bytes)\n", header -> e_shoff); 
 
  /*5*/
  printf("  Number of section headers:\t\t%d\n", header -> e_shnum); 

  /*6*/
  printf("  Size of section headers::\t\t%d (bytes)\n", header -> e_shentsize); 
  
  /*7*/
  printf("  Start of program headers:\t\t%d (bytes)\n", header -> e_phoff); 
 
  /*8*/
  printf("  Number of program headers:\t\t%d\n", header -> e_phnum); 

  /*9*/
  printf("  Size of program headers:\t\t%d (bytes)\n", header -> e_phentsize); 


}

void print_sections(Elf32_Shdr * sectionT, unsigned int s_num)
{
  int iter;
  Elf32_Shdr * curr_sec;
  char * name;
  unsigned int addr, offset, size;
  
  printf("[Nr] Name              Addr     Off    Size\n");
  for(iter = 0; iter < s_num; iter++)
  {
    curr_sec = sectionT + iter;
    name = (char *)(curr_sec -> sh_name);
    printf("Name: %s", name);
    addr = curr_sec -> sh_addr;
    printf("Address: %08x", addr);
    offset = curr_sec -> sh_offset;
    printf("Name: %s", name);
    size = curr_sec -> sh_size;
    printf("Name: %s", name);
    
   printf("[%d]  %s\t\t%08x \t%06x\t%06x\n", iter, name, addr, offset, size);
  }
}


