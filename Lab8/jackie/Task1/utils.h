#include <sys/mman.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#include "elf.h"


void print_header(Elf32_Ehdr * header);
void print_sections(Elf32_Shdr * sectionT, unsigned int s_num);