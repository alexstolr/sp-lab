#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <elf.h>

#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <sys/mman.h>
#include <fcntl.h> /*open*/

void checkArgs(int argc){
  if(argc != 3){
    printf("%s\n","Error: Incorrect number of parameters received");
    exit(1);
  }
}

/*checks if ELF format.*/
int checkELF(Elf32_Ehdr * header1,Elf32_Ehdr * header2){
  if( 'E' !=header1->e_ident[EI_MAG1] || 'L' !=header1->e_ident[EI_MAG2] || 'F' !=header1->e_ident[EI_MAG3] || ELFCLASS32 !=header1->e_ident[EI_CLASS])
    return 0;
  printf("\nFile 1 format : %c%c%c32\n",header1->e_ident[EI_MAG1],header1->e_ident[EI_MAG2],header1->e_ident[EI_MAG3]);
  
  if( 'E' !=header2->e_ident[EI_MAG1] || 'L' !=header2->e_ident[EI_MAG2] || 'F' !=header2->e_ident[EI_MAG3] || ELFCLASS32 !=header2->e_ident[EI_CLASS])
    return 0;
  printf("\nFile 2 format : %c%c%c32\n",header2->e_ident[EI_MAG1],header2->e_ident[EI_MAG2],header2->e_ident[EI_MAG3]);
  
  return 1;
}

int main(int argc, char ** argv){
  checkArgs(argc);
  int fd1;
  int fd2;
  void * map_start1;
  void * map_start2;
  struct stat fd_stat1; /* this is needed to  the size of the file */
  struct stat fd_stat2;
  Elf32_Ehdr * header1;
  Elf32_Ehdr * header2;
  
  
  /*start init*/
  if( (fd1 = open(argv[1], O_RDWR)) < 0 ){
    perror("Error opening file1");
    exit(-1);
  }
  if( (fd2 = open(argv[2], O_RDWR)) < 0 ){
    perror("Error opening file2");
    exit(-1);
  }
  if( fstat(fd1, &fd_stat1) != 0 ){
    perror("stat 1 failed");
    exit(-1);
  }
  if( fstat(fd2, &fd_stat2) != 0 ){
    perror("stat 2 failed");
    exit(-1);
  }
  if( (map_start1 = mmap(0, fd_stat1.st_size, PROT_READ | PROT_WRITE , MAP_SHARED, fd1, 0)) < 0 ){
    perror("mmap1 failed");
    exit(-4);
  }
  if( (map_start2 = mmap(0, fd_stat2.st_size, PROT_READ | PROT_WRITE , MAP_SHARED, fd2, 0)) < 0 ){
    perror("mmap2 failed");
    exit(-4);
  }
  header1 = (Elf32_Ehdr *) map_start1;
  header2 = (Elf32_Ehdr *) map_start2;
  if((checkELF(header1,header2)) == 0){
    printf("Error, some of the files you have chosen are not of 'ELF32' format!\n");
    exit(1);
  }
  /*end init*/
  
  /*TODO need to check for all symbols in symbol name if equals to main using strcmp*/
  int main1flag = 0, main2flag = 0;;
  Elf32_Shdr * sections1= (Elf32_Shdr *)(map_start1 + header1->e_shoff);
  Elf32_Sym * symTable1;
  int num_of_section_headers1 = header1->e_shnum;
  char * symbol_string_table1;
  char * section_string_table1;
  section_string_table1 = (char *)(map_start1 + sections1[header1->e_shstrndx].sh_offset);
  int i,j;
  printf("TEST!!\n");
  for(i=0; i < num_of_section_headers1; i++){
    if(sections1[i].sh_type== SHT_SYMTAB || sections1[i].sh_type==SHT_DYNSYM){
      Elf32_Sym * symTable1 = (Elf32_Sym *)((char *)(map_start1 + sections1[i].sh_offset));
      symbol_string_table1 = (char *)(map_start1 + sections1[sections1[i].sh_link].sh_offset);
      for ( j=0; j<(sections1[i].sh_size/sections1[i].sh_entsize) ; j++ ){
	printf("%-20s\n",section_string_table1 + sections1[symTable1[j].st_shndx].sh_name);
      }
    }
  }
  
  return 0;
}