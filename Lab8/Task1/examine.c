#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <elf.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <sys/mman.h>
#include <fcntl.h> /*open*/


/*headers*/
void testPrint(int printIndex,char * string, void * address,int addressFlag);
void checkArgs(int argc);

/*headers*/



/*prints tests,strings and addresses.
 * to print int: testPrint(i,0,0,0);
 * to print string: testPrint(0,string,0,0);
 * to print address: testPrint(0,0,address,1);
 */
void testPrint(int printIndex,char * string, void * address,int addressFlag){
  if(printIndex != 0)
    printf("TEST %d\n",printIndex);
  if(string != 0)
    printf("%s\n",string);
  if(addressFlag == 1)
    printf("%p\n",address);
}

void checkArgs(int argc){
  if(argc !=2){
    printf("Error, Wrong number of arguments\n");
    exit(1);
  }
}

/*checks if ELF format.*/
int checkELF(int i,Elf32_Ehdr * header){
  
  if( 'E' !=header->e_ident[EI_MAG1] || 'L' !=header->e_ident[EI_MAG2] || 'F' !=header->e_ident[EI_MAG3] || ELFCLASS32 !=header->e_ident[EI_CLASS])
    return 0;
  printf("\nFile format is : %c%c%c32\n",header->e_ident[EI_MAG1],header->e_ident[EI_MAG2],header->e_ident[EI_MAG3]);
  return 1;
}

int main(int argc, char ** argv){
  checkArgs(argc);
  int fd;
  void *map_start; /* will point to the start of the memory mapped file */
  struct stat fd_stat; /* this is needed to  the size of the file */
  Elf32_Ehdr * header; /* this will point to the header structure */
  Elf32_Shdr * section;
  Elf32_Phdr * progTable;
  int num_of_section_headers;
  int num_of_program_headers;
  char * section_string_table;
  
  if( (fd = open(argv[1], O_RDWR)) < 0 ){
    perror("error in open");
    exit(-1);
  }
  if( fstat(fd, &fd_stat) != 0 ){
    perror("stat failed");
    exit(-1);
  }
  if( (map_start = mmap(0, fd_stat.st_size, PROT_READ | PROT_WRITE , MAP_SHARED, fd, 0)) < 0 ){
    perror("mmap failed");
    exit(-4);
  }
  
  /* now, the file is mapped starting at map_start.
   * all we need to do is tell *header to point at the same address:
   */
  
  header = (Elf32_Ehdr *) map_start;
  /* now we can do whatever we want with header!!!!
   * for example, the number of section header can be obtained like this:
   */
  if((checkELF(4,header)) == 0){
    printf("Error, the file you have chosen is not of 'ELF32' format!\n");
    exit(1);
  }
    /*&&&&&&&&&&&&&&&&&&&&&&&&&  Task0  &&&&&&&&&&&&&&&&&&&&&&&&&*/
  printf("The data encoding scheme of the object file: %d",header->e_ident[EI_DATA]);
  if(1 == header->e_ident[EI_DATA])
    printf(" --> 2's complement, little endian\n");
  else printf("\n");
  printf("Entry point: %p\n",(void *)header->e_entry);
  printf("section header file offset :%d\n",header->e_shoff);
  printf("number of section header entries :%d\n",header->e_shnum);
  
  printf("\n");
  section = (Elf32_Shdr *)(map_start + header->e_shoff);
  
  num_of_section_headers = header->e_shnum;
  
  int i;
  for(i = 0; i< num_of_section_headers ; i++){
    printf("size of section %d is: %d\n",i,section[i].sh_size);
  }
  printf("\n");
  printf("header table file offset :%d\n",header->e_phoff);
  printf("number of program header entries :%d\n",header->e_phnum);
  
  printf("\n");
  progTable = (Elf32_Phdr *)(map_start + header->e_phoff);
  num_of_program_headers = header->e_phnum;
  int j;
  for(j = 0; j< num_of_program_headers ; j++){
    printf("size of program header entry %d is: %d\n",j,progTable[j].p_filesz);
  }
  /*&&&&&&&&&&&&&&&&&&&&&&&&&  Task0  &&&&&&&&&&&&&&&&&&&&&&&&&*/
  
  section_string_table = (char *)(map_start + section[header->e_shstrndx].sh_offset);
  
  int i2;
  printf("\n[Nr]     Name              Addr             Off            Size   \n");
  for(i2 = 0; i2< num_of_section_headers ; i2++){
    printf("[%d]     %s              %08x            %06x           %06x\n" , i2,  section_string_table+section[i2].sh_name , section[i2].sh_addr , section[i2].sh_offset , section[i2].sh_size);
  }
  
  
  
  
  
  
  
  
  /* now, we unmap the file */
  munmap(map_start, fd_stat.st_size);
  
  return 0;
}
