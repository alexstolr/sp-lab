#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>
#include <limits.h> /* PATH_MAX */
#include "LineParser.h"

#ifndef PATH_MAX
#define MAX_LINE_SIZE 2048 
#endif
#define PATH_MAX 255

/*receives a parsed line and invokes the command using the proper system call */
void execute(cmdLine * pCmdLine);

int main(void){
  while(1){
    char dirname[PATH_MAX + 1]; /* To be passed to getcwd system call. */
    
    /* Use getcwd to get the name of the current working directory. */
    if (getcwd(dirname, PATH_MAX) == NULL) {
      fprintf(stderr, "Could not obtain current working directory.\n");
      exit(1);
    }
    else {
      printf("Current working directory: %s\n", dirname);
    }
    
    char lineBuf[MAX_LINE_SIZE + 1];
    if(!fgets(lineBuf,MAX_LINE_SIZE,stdin))
      break;
    if(strcmp("quit",lineBuf)==0)
      exit(0);
    cmdLine * line= parseCmdLines(lineBuf);
    execute(line);
    freeCmdLines(line);
  }
  return 0;
}

void execute(cmdLine * pCmdLine){
  
  if( (execvp((pCmdLine->arguments)[0],pCmdLine->arguments)) == -1 ){
    /*printf("Oh no, something went wrong! %s\n", strerror(errno));*/
    perror("Error ");
  }
}
