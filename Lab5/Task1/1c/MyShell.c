#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <errno.h>
#include <sys/wait.h>
#include <string.h>
#include <sys/types.h>  /* Primitive System Data Types */ 
#include <limits.h> /* PATH_MAX */
#include "LineParser.h"

#ifndef PATH_MAX
#define MAX_LINE_SIZE 2048 
#endif
#define PATH_MAX 255

/*receives a parsed line and invokes the command using the proper system call */
void execute(cmdLine * pCmdLine);

int changeDir(cmdLine * line);

void execute(cmdLine * pCmdLine){
  pid_t pid;
  int status;
  
  pid = fork();
  if(pid >= 0){
    if(pid == 0){	/*child process*/
      if( (execvp((pCmdLine->arguments)[0],pCmdLine->arguments)) == -1 ){
	perror("Error ");
	exit(0); 
      }
    }
    else{/*parent process*/
      if((pCmdLine->blocking) == 1){
	while(waitpid(-1, &status, 0) != pid);
      }
    }
  }
  else{
    perror("fork"); /* display error message */
    exit(0); 
  }
}

int changeDir(cmdLine * line){
  int isCD = 0;
  if(strcmp((line->arguments)[0],"cd")==0){
    isCD =1;
    if((chdir((line->arguments)[1])) == -1){
      perror("Error ");
    }
  }
  return isCD;
}

int main(void){
  
  char dirname[PATH_MAX + 1]; /* To be passed to getcwd system call. */
  char lineBuf[MAX_LINE_SIZE + 1];
  
  while(1){
    if (getcwd(dirname, PATH_MAX) == NULL) { /*get current working directory*/
      fprintf(stderr, "Could not obtain current working directory.\n");
      exit(1);
    }
    else {
      printf("Current working directory: %s\n", dirname);
    }
    if(!gets(lineBuf))
      exit(0);
    if(strcmp("quit",lineBuf)==0)
      break;
    cmdLine * line= parseCmdLines(lineBuf);
    if(changeDir(line) == 0){
      execute(line);
    }
    freeCmdLines(line);
  }
  return 0;
}







/*from pdf in lecture!*/
/*
 w *********hile*(t*rue) {
   get_line(buf, stdin);
   if(feof(stdin))
     exit(0);
   parse(buf, path, argv, envp);
   if(!(pid=fork()))
     execve(path, argv, envp);
   wait_for_child(pid);
 }
 */


/*
 * #include <stdio.h>
 # ***********i****nclude <stdlib.h>
 
 #define MAX_LENGTH 1024
 
 int main(int argc, char *argv[]) {
   char line[MAX_LENGTH];
   
   while (1) {
     printf("$ ");
     if (!fgets(line, MAX_LENGTH, stdin)) break;
     system(line);
 }
 
 return 0;
 }
 */