#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <errno.h>
#include <sys/wait.h>
#include <string.h>
#include <sys/types.h>  /* Primitive System Data Types */ 
#include <limits.h> /* PATH_MAX */
#include "LineParser.h"

#define MAX_LINE_SIZE 2048
#define MAX_COMMANDS 10 
#define PATH_MAX 255


/*receives a parsed line and invokes the command using the proper system call */
void execute(cmdLine * pCmdLine){
  pid_t pid;
  int status;
  
  pid = fork();
  if(pid >= 0){
    if(pid == 0){	/*child process*/
      if( (execvp((pCmdLine->arguments)[0],pCmdLine->arguments)) == -1 ){
	exit(0); 
      }
    }
    else{/*parent process*/
      if((pCmdLine->blocking) == 1){
	while(waitpid(-1, &status, 0) != pid);
      }
    }
  }
  else{
    perror("fork"); /* display error message */
    exit(0);
  }
}


/*changes current directory*/
void changeDir(cmdLine * line){
  if(strcmp((line->arguments)[0],"cd")==0){
    if((chdir((line->arguments)[1])) == -1){
      perror("Error ");
    }
  }
}


/*prints command history*/
void printComHist(char ** commandHistory,int startIndex,int commandsCount){
  int i,j;
  for(i=0 ; i< commandsCount; i++){
    printf("%s%d%s","    ", i, "  ");
    for(j = 0; commandHistory[(startIndex = i)%MAX_COMMANDS][j] != '\0'; j++){
      printf("%c",commandHistory[(startIndex = i)%MAX_COMMANDS][j]);
    }
    printf("\n");
  }
}


/*adds commands to command history log*/
void addCommToLog(char ** commandHistory,char * lineBuf, int * startIndex, int * commandsCount){
  int i;
  if( (*commandsCount) < MAX_COMMANDS){
    commandHistory[(*commandsCount)] = malloc(MAX_LINE_SIZE);
    for( i = 0 ;  i<strlen(lineBuf) ; i++)
      commandHistory[(*commandsCount)][i] = lineBuf[i];
    commandHistory[(*commandsCount)][i] = '\0';
    (*commandsCount)++;
  }
  else{
    if( (*startIndex) == MAX_COMMANDS)
      (*startIndex) = 0;
    for (i=0 ; i<strlen(lineBuf) ; i++)
      commandHistory[(*startIndex)][i] = lineBuf[i];
    commandHistory[(*startIndex)][i] = '\0';
    (*startIndex)++;
  }
}

/*Display a prompt - the current working directory*/
void getCurWorkDir(char * dirname){
  if (getcwd(dirname, PATH_MAX) == NULL) { /*get current working directory*/
    fprintf(stderr, "Could not obtain current working directory.\n");
    exit(1);
  }
  else {
    printf("Current working directory: %s\n", dirname);
  }
}

void reuseCommand(char * lineBuf, int commandsCount){
  if(lineBuf[0] == '!'){
      if(line[1] > commandsCount)
	printf("command index out of bound, try again..");
      else{
	
      }
    }
}


int main(void){
  char dirname[PATH_MAX + 1]; /* To be passed to getcwd system call. */
  char lineBuf[MAX_LINE_SIZE + 1];
  int startIndex = 0 , commandsCount = 0;
  char * commandHistory[MAX_COMMANDS];
  while(1){
    getCurWorkDir(dirname);
    if(!gets(lineBuf))
      exit(0);
    if(strcmp("quit",lineBuf)==0)
      exit(0);
    addCommToLog(commandHistory, lineBuf, &startIndex, &commandsCount);
    if(strcmp("log",lineBuf)==0)
      printComHist(commandHistory,startIndex,commandsCount);
    cmdLine * line= parseCmdLines(lineBuf);
    changeDir(line);
    execute(line);
    freeCmdLines(line);
  }
  return 0;
}
