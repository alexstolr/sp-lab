#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>
#include <sys/types.h>  /* Primitive System Data Types */ 
#include <limits.h> /* PATH_MAX */
#include "LineParser.h"

#ifndef PATH_MAX
#define MAX_LINE_SIZE 2048 
#endif
#define PATH_MAX 255

/*receives a parsed line and invokes the command using the proper system call */
void execute(cmdLine * pCmdLine){
  pid_t pid;
  pid = fork();
  if(pid >= 0){
    if(pid == 0){	/*child process*/
      if( (execvp((pCmdLine->arguments)[0],pCmdLine->arguments)) == -1 ){
	perror("Error ");
	exit(0); 
      }
    }
    else{/*parent process*/
      ;
    }
  }
  else{
    perror("fork"); /* display error message */
    exit(0); 
  }
}

int main(void){
  char dirname[PATH_MAX + 1]; /* To be passed to getcwd system call. */
  char lineBuf[MAX_LINE_SIZE + 1];
  while(1){
    if (getcwd(dirname, PATH_MAX) == NULL) { /*get current working directory*/
      fprintf(stderr, "Could not obtain current working directory.\n");
      exit(1);
    }
    if(!gets(lineBuf))
      exit(0);
    if(strcmp("quit",lineBuf)==0)
      break;
    cmdLine * line= parseCmdLines(lineBuf);
    execute(line);
    freeCmdLines(line);
  }
  return 0;
}
