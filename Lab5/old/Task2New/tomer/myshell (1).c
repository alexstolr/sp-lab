#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include "LineParser.h"
#include <errno.h>
#include <string.h>
#include <sys/wait.h>
#include <sys/types.h>

#define PATH_MAX 255
#define MAX_COMMANDS 10
#define MAX_INPUT 2048

typedef struct bindingList bind;

struct bindingList{
	char * var;
	char * val;
	bind * next;
};


void execute(cmdLine * pCmdLine){
	int status;
	pid_t pid = fork();
	if(pid < 0 ) perror("Error: ");
	if (pid == 0 ){
		if( (execvp( (pCmdLine->arguments)[0] , (pCmdLine -> arguments) ) ) == -1 ){
		      exit(1);
		}
	}
	if( pid > 0){
		if( (pCmdLine->blocking) == 1){
			while( wait(&status) != pid ) ;    
		}
	}
	
}

void changeDir(char * path){
	  if( (chdir(path) == -1) ){
		  perror("Error: ");
	  }
}

void addToHistory(char * line , char ** history , int * start , int * commandsNum){
	int i;
	if( (*commandsNum) < MAX_COMMANDS){
		history[(*commandsNum)] = malloc(strlen(line)+1);
		for( i = 0 ;  i<strlen(line) ; i++)
			history[(*commandsNum)][i] = line[i];
		history[(*commandsNum)][i] = '\0';
		(*commandsNum)++;
	}
	else{
		if( (*start) == MAX_COMMANDS) (*start) = 0;
		for (i=0 ; i<strlen(line) ; i++)
			history[(*start)][i] = line[i];
		history[(*start)][i] = '\0';
		(*start)++;
	}
}

void printLog(char ** history , int  start , int commandsNum){
	  int i , j , t ;
	  if ( commandsNum >= MAX_COMMANDS - 1){
		for(i = 0 ; i < commandsNum ;i++){
			t= (( i + start)%MAX_COMMANDS);
			printf("#%d      " , i);   
			for (j = 0 ; history[t][j] != '\0'   ; j++){
				printf("%c" , history[t][j] );
			}
			printf("\n");
		}
	  }
	  else{
	      	for(i = 0 ; i < commandsNum ; i++){
			printf("#%d      " , i);      
			for (j=0 ; history[i][j] != '\0' ; j++){
				printf("%c" , history[i][j]);
			}
			printf("\n");
		}
	  }
}

bind * assign(char * var , char * val){
	bind * ans = malloc(sizeof(bind)) ;
	ans->var = malloc(sizeof(var)+1);
	ans->val = malloc(sizeof(val) + 1);
	ans->next = 0;
	strcpy( ans->var , var);
	strcpy( ans->val , val);
	return ans;
}

bind * append(bind * first , bind * last){
	bind * temp = first;
	while( (temp->next != 0) ){
		temp = temp->next ;
	}  
	temp->next = last;   
	return first;
}

void printList(bind * first){
	while(first != 0){
		printf("%s      ----     %s\n" , first->var , first -> val);
		first = first -> next;
	}
	printf("\n");
}

bind * unassign(bind ** first , char * var , int * firstFlag){
	bind * ans = *first;
	bind * curr;
	bind * prev = NULL;
	int found = 0;
	if (first == 0) fprintf(stderr,"The env is empty\n");
	else{
		for ( curr = *first ; (found != 1) && curr != NULL ; prev = curr , curr = (curr -> next) ){
			if ( strcmp(curr-> var , var) == 0){
			      found = 1;
			      if (prev == NULL){
				      ans = curr->next;
				      if (ans == NULL) (*firstFlag)=1;
			      }
			      else (prev->next) = (curr->next) ; 
			      free(curr->var);
			      free(curr-> val);
			      free(curr);
			}
		}
	}
	if(!found)fprintf(stderr,"The variable %s is'nt exist in the env\n", var);
	return ans;
}


/* return the node if found and null else*/

bind * search(bind * first , char * var){
	int found = 0;
	bind * ans = NULL;
	while( (found != 1) && first != NULL){
		if ( strcmp(first-> var , var) == 0){
			ans = first;
			found = 1;
		}
		first=first->next;
	}
	return ans;
}

bind * run(char * line, char ** history , int * start , int * commandsNum , bind * first , int * firstFlag ){
	addToHistory(line , history , start , commandsNum);
	if ( (strcmp (line , "log") == 0) )
		printLog(history, (*start) , (*commandsNum) );
	else if ( (strcmp(line , "quit")) == 0)
	      exit(0);
	cmdLine * cmdL = parseCmdLines(line);
	if ( strcmp("cd",(cmdL->arguments)[0]) == 0)
		changeDir( (cmdL->arguments)[1] );
	else if ( strcmp("assign" , (cmdL -> arguments)[0])  == 0){
		 bind * exi = search(first , (cmdL -> arguments)[1]);
		 if (exi != NULL) unassign(&first, (cmdL -> arguments)[1], firstFlag);
		 bind * temp = assign( (cmdL -> arguments)[1] , (cmdL -> arguments)[2] );
		 if ( *(firstFlag) ) {
			first = temp;
			(*firstFlag) = 0;
		 }
		 else{
		  first = append(first , temp);
		 }
	}
	else if ( strcmp("unassign" , (cmdL -> arguments)[0])  == 0){
		first = unassign(&first , (cmdL -> arguments)[1] , firstFlag);
	}
	else if ( strcmp("env" , (cmdL -> arguments)[0])  == 0){
		printList(first);
	}
	else execute(cmdL);
	freeCmdLines(cmdL);
	return first;
}


int main(int argc , char **argv){
	char path[PATH_MAX+1];
	char line[MAX_INPUT+1];
	int commandsNum=0 , start = 0;
	char *history[MAX_COMMANDS]; /*TODO Free MEMORY*/
	bind * first = 0;
	int firstFlag = 1;
	while(1){
		if ( getcwd(path,PATH_MAX) == NULL){
		    fprintf(stderr , "Could not obtain current working directory \n");
		    exit(1);
		}if(strcmp("quit",lineBuf)==0)
    exit(0);
  addCommToLog(commandHistory, lineBuf, startIndex, commandsCount);
  if(strcmp("log",lineBuf)==0)
    printComHist(commandHistory,*startIndex,*commandsCount);
  cmdLine * line= parseCmdLines(lineBuf);
  if(strncmp("$",line->arguments[1],1)==0){
    char * nameToCopy = malloc(strlen(line->arguments[1]));
    strncpy(nameToCopy,((line->arguments[1])+1),strlen(line->arguments[1]));
    strcpy(line->arguments[1],getValue(nameToCopy,sPairList));
  }
		printf("The current directory is %s\n" , path);
		gets(line);
		if (strlen(line) > 1){
			if (line[0] == '!'){
				int num = line[1] - '0';
				if ( (num >=0) && (num < commandsNum)){
					first = run(history[num] , history , &start , &commandsNum , first , &firstFlag);
				}
				else fprintf(stderr,"OutOfBound command %d \n", num);
			}
			else first = run(line, history , &start , &commandsNum, first , &firstFlag);
		}
	}
}





if(strcmp("quit",lineBuf)==0)
    exit(0);
  addCommToLog(commandHistory, lineBuf, startIndex, commandsCount);
  if(strcmp("log",lineBuf)==0)
    printComHist(commandHistory,*startIndex,*commandsCount);
  cmdLine * line= parseCmdLines(lineBuf);
  if(strncmp("$",line->arguments[1],1)==0){
    char * nameToCopy = malloc(strlen(line->arguments[1]));
    strncpy(nameToCopy,((line->arguments[1])+1),strlen(line->arguments[1]));
    strcpy(line->arguments[1],getValue(nameToCopy,sPairList));
  }