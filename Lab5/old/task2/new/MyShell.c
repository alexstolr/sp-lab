#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <errno.h>
#include <sys/wait.h>
#include <string.h>
#include <sys/types.h>  /* Primitive System Data Types */ 
#include <limits.h> /* PATH_MAX */
#include "LineParser.h"

#define MAX_LINE_SIZE 2048
#define MAX_COMMANDS 10 
#define PATH_MAX 255


typedef struct stringPair
{
  char *  name; /* a variable name that appears in the command line - example: "i" */
  char *  value;	/* value of name. example: when command is "$i" output is "hello" */
  struct stringPair * next;	/* next stringPair in chain */
} stringPair;

/*Headers*/
stringPair * checkReuseCommand(stringPair * sPairList, char ** commandHistory,char * lineBuf, int * startIndex, int * commandsCount);
stringPair * getLink(stringPair * first , char * name);
stringPair * assign(char * name, char * value, stringPair * sPairList);
void unassign(stringPair * sPairList, char * name);
void env(stringPair * sPairList);
stringPair * append(stringPair * head , stringPair * last);
char * getValue(char * name, stringPair * sPairList);


/*receives a parsed line and invokes the command using the proper system call */
void execute(cmdLine * pCmdLine){
  pid_t pid;
  int status;
  
  pid = fork();
  if(pid >= 0){
    if(pid == 0){	/*child process*/
      if( (execvp((pCmdLine->arguments)[0],pCmdLine->arguments)) == -1 ){
	exit(0); 
      }
    }
    else{/*parent process*/
      if((pCmdLine->blocking) == 1){
	while(waitpid(-1, &status, 0) != pid);
      }
    }
  }
  else{
    perror("fork"); /* display error message */
    exit(0);
  }
}


/*changes current directory*/
void changeDir(cmdLine * line){
  if(strcmp((line->arguments)[0],"cd")==0){
    if((chdir((line->arguments)[1])) == -1){
      perror("Error ");
    }
  }
}


/*prints command history*/
void printComHist(char ** commandHistory,int startIndex,int commandsCount){
  int i,j;
  for(i=0 ; i< commandsCount; i++){
    printf("%s%d%s","    ", i, "  ");
    for(j = 0; commandHistory[(startIndex = i)%MAX_COMMANDS][j] != '\0'; j++){
      printf("%c",commandHistory[(startIndex = i)%MAX_COMMANDS][j]);
    }
    printf("\n");
  }
}


/*adds commands to command history log*/
void addCommToLog(char ** commandHistory,char * lineBuf, int * startIndex, int * commandsCount){
  int i;
  if( (*commandsCount) < MAX_COMMANDS){
    commandHistory[(*commandsCount)] = malloc(MAX_LINE_SIZE);
    for( i = 0 ;  i<strlen(lineBuf) ; i++)
      commandHistory[(*commandsCount)][i] = lineBuf[i];
    commandHistory[(*commandsCount)][i] = '\0';
    (*commandsCount)++;
  }
  else{
    if( (*startIndex) == MAX_COMMANDS)
      (*startIndex) = 0;
    for (i=0 ; i<strlen(lineBuf) ; i++)
      commandHistory[(*startIndex)][i] = lineBuf[i];
    commandHistory[(*startIndex)][i] = '\0';
    (*startIndex)++;
  }
}

/*Display a prompt - the current working directory*/
void getCurWorkDir(char * dirname){
  if (getcwd(dirname, PATH_MAX) == NULL) { /*get current working directory*/
    fprintf(stderr, "Could not obtain current working directory.\n");
    exit(1);
  }
  else {
    printf("Current working directory: %s\n", dirname);
  }
}

stringPair * checkInVars(char * lineBuf, cmdLine * line, stringPair * sPairList){
  if(strcmp("assign",line->arguments[0]) == 0){
    char * name = line->arguments[1];
    char * value = line->arguments[2];
    stringPair * tmp = getLink(sPairList,name);
    printf("%p\n",tmp);
    if(tmp!=0){
      printf("%s\n",tmp->name);
    printf("%s\n",tmp->value);
    }
    stringPair * tail = assign(name, value, tmp);
    sPairList = append(sPairList,tail);
  }
  else if(strcmp("unassign",line->arguments[0]) == 0){
    char * name = line->arguments[1];
    unassign(sPairList, name);
  }
  else if(strcmp("env",line->arguments[0]) == 0){
    env(sPairList); 
    strcpy( line->arguments[0] , "  ");
  }
  return sPairList;
}

stringPair * runCommand(stringPair * sPairList, char ** commandHistory, char * lineBuf, int * startIndex, int * commandsCount){
  if(strcmp("quit",lineBuf)==0)
    exit(0);
  addCommToLog(commandHistory, lineBuf, startIndex, commandsCount);
  if(strcmp("log",lineBuf)==0)
    printComHist(commandHistory,*startIndex,*commandsCount);
  cmdLine * line= parseCmdLines(lineBuf);
  if(line->argCount > 1){
    if(strncmp("$",line->arguments[1],1)==0){
    char * nameToCopy = malloc(strlen(line->arguments[1]));
    strncpy(nameToCopy,((line->arguments[1])+1),strlen(line->arguments[1]));
    strcpy(line->arguments[1],getValue(nameToCopy,sPairList));
    }
  }
  sPairList = checkInVars(lineBuf,line,sPairList);
  changeDir(line);
  execute(line);
  freeCmdLines(line);
  return sPairList;
}

stringPair * checkReuseCommand(stringPair * sPairList, char ** commandHistory,char * lineBuf, int * startIndex, int * commandsCount){
  if(lineBuf[0] == '!'){
    int num = lineBuf[1] - '0';
    if(num > (*commandsCount)){
      printf("command index out of bound, try again..\n");
    }
    else{
      char * reusedLine = commandHistory[num];
      sPairList = runCommand(sPairList, commandHistory, reusedLine, startIndex, commandsCount);
    }
  }
  else{
    sPairList = runCommand(sPairList, commandHistory, lineBuf, startIndex, commandsCount);
  }
  return sPairList;
}

void unassign(stringPair * sPairList, char * name){
  int found = 0;  
  stringPair * currSPair = sPairList;
  stringPair * prevSPair = NULL;
  while(found != 1 || currSPair != NULL){
    if(strncmp(currSPair->name,name,strlen(name)) == 0){/*found it*/
      if(prevSPair == NULL){/* Fix beginning pointer. */
	sPairList = currSPair->next;
	
      }
      else{
	free(currSPair->name);
	free(currSPair->value);
	prevSPair->next = currSPair->next;
	found = 1;
	free(currSPair);
	break;
      }
    }
    prevSPair = currSPair;
    currSPair = currSPair->next;
  }
  if(found == 0)
    printf("Error! cannot remove variable %s since it was not found.\n", name);
  else
    printf("variable %s has been removed successfully.\n", name);
}

/*
void unassign(stringPair * sPairList, char * name){
  int found = 0;  
  stringPair * currSPair = sPairList;
  stringPair * prevSPair = sPairList;
  while(found != 1 || currSPair != NULL){
    if(strncmp(currSPair->name,name,strlen(name)) == 0){
      free(currSPair->name);
      free(currSPair->value);
      prevSPair->next = currSPair->next;
      found = 1;
      free(currSPair);
      break;
    }
    prevSPair = currSPair;
    currSPair = currSPair->next;
  }
  if(found == 0)
    printf("Error! cannot remove variable %s since it was not found.\n", name);
  else
    printf("variable %s has been removed successfully.\n", name);
}*/



stringPair * assign(char * name, char * value, stringPair * link){
  if(link != NULL){/*if such link does not exit in the list*/
    printf("link != NULL\n");
    unassign(link,name);
    stringPair * newLink = malloc(sizeof(stringPair)) ;
    newLink->name = malloc(sizeof(name)+1);
    newLink->value = malloc(sizeof(value) + 1);
    newLink->next = NULL;
    strcpy( newLink->name , name);
    strcpy( newLink->value , value);
    printf("value '%s' has been reassigned to '%s' successfully.\n",value, name);
    return newLink;
  }
  else{
    stringPair * newLink = malloc(sizeof(stringPair)) ;
    newLink->name = malloc(sizeof(name)+1);
    newLink->value = malloc(sizeof(value) + 1);
    newLink->next = NULL;
    strcpy( newLink->name,name);
    strcpy( newLink->value,value);
    printf("value '%s' has been assigned to '%s' successfully.\n",value, name);
    return newLink;
  }
}

/*finds a link in the linked list by name and returns it. if cannot be found returns NULL.*/
stringPair * getLink(stringPair * first , char * name){
  int found = 0;
  stringPair * link = NULL;
  stringPair * tmp = first;
  while( (found != 1) && tmp != NULL){
    if ( strcmp(tmp->name , name) == 0){
      return tmp;
    }
    tmp=tmp->next;
  }
  return link;
}

/*appends a link to the end of the list
 * returns the head of the list
 */
stringPair * append(stringPair * head , stringPair * last){
  stringPair * curr = head;
  if(curr == 0){
    return last;
  }
  while( (curr->next != 0) ){
    curr = curr->next ;
  }  
  curr->next = last; 
  return head;
}


/*prints all current associations in the environment*/
void env(stringPair * sPairList){
  stringPair * currSPair = sPairList;
  while(currSPair != NULL){
    printf("< %s : %s >\n",currSPair->name,currSPair->value);
    currSPair = currSPair->next;
  }
}

char * getValue(char * name, stringPair * sPairList){
  stringPair * link =  getLink(sPairList , name);
  if(link == 0){
    printf("Fatal Error! the name you have entered does not have a value!!!\n");
    exit(1);
  }
  return link->value;
}

int main(void){
  char dirname[PATH_MAX + 1]; /* To be passed to getcwd system call. */
  char lineBuf[MAX_LINE_SIZE + 1];
  int startIndex = 0 , commandsCount = 0;
  char * commandHistory[MAX_COMMANDS];
  stringPair * sPairList = 0;
  while(1){
    getCurWorkDir(dirname);
    if(!gets(lineBuf))
      exit(0);
    if(strlen(lineBuf) > 1){
      sPairList = checkReuseCommand(sPairList, commandHistory, lineBuf, &startIndex, &commandsCount);
    }
    else
      printf("Error! illegal input..");
  }
  return 0;
}

