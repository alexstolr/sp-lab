#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <errno.h>
#include <sys/wait.h>
#include <string.h>
#include <sys/types.h>  /* Primitive System Data Types */ 
#include <limits.h> /* PATH_MAX */
#include "LineParser.h"

#define MAX_LINE_SIZE 2048
#define MAX_COMMANDS 10 
#define PATH_MAX 255


typedef struct stringPair
{
  char *  name; /* a variable name that appears in the command line - example: "i" */
  char *  value;	/* value of name. example: when command is "$i" output is "hello" */
  struct stringPair * next;	/* next stringPair in chain */
} stringPair;

/*Headers*/
stringPair * checkInVars(char * lineBuf, cmdLine * line, stringPair * sPairList);
void vUnassign(stringPair * sPairList, char * name);
void vAssign(char * name, char * value, stringPair * sPairList);
void vEnv(stringPair * sPairList);
stringPair * append(stringPair * list , stringPair * link);
stringPair * getLink(stringPair * sPairList, char * name, char * value);


/*receives a parsed line and invokes the command using the proper system call */
void execute(cmdLine * pCmdLine){
  pid_t pid;
  int status;
  
  pid = fork();
  if(pid >= 0){
    if(pid == 0){	/*child process*/
      if( (execvp((pCmdLine->arguments)[0],pCmdLine->arguments)) == -1 ){
	exit(0); 
      }
    }
    else{/*parent process*/
      if((pCmdLine->blocking) == 1){
	while(waitpid(-1, &status, 0) != pid);
      }
    }
  }
  else{
    perror("fork"); /* display error message */
    exit(0);
  }
}


/*changes current directory*/
void changeDir(cmdLine * line){
  if(strcmp((line->arguments)[0],"cd")==0){
    if((chdir((line->arguments)[1])) == -1){
      perror("Error ");
    }
  }
}


/*prints command history*/
void printComHist(char ** commandHistory,int startIndex,int commandsCount){
  int i,j;
  for(i=0 ; i< commandsCount; i++){
    printf("%s%d%s","    ", i, "  ");
    for(j = 0; commandHistory[(startIndex = i)%MAX_COMMANDS][j] != '\0'; j++){
      printf("%c",commandHistory[(startIndex = i)%MAX_COMMANDS][j]);
    }
    printf("\n");
  }
}


/*adds commands to command history log*/
void addCommToLog(char ** commandHistory,char * lineBuf, int * startIndex, int * commandsCount){
  int i;
  if( (*commandsCount) < MAX_COMMANDS){
    commandHistory[(*commandsCount)] = malloc(MAX_LINE_SIZE);
    for( i = 0 ;  i<strlen(lineBuf) ; i++)
      commandHistory[(*commandsCount)][i] = lineBuf[i];
    commandHistory[(*commandsCount)][i] = '\0';
    (*commandsCount)++;
  }
  else{
    if( (*startIndex) == MAX_COMMANDS)
      (*startIndex) = 0;
    for (i=0 ; i<strlen(lineBuf) ; i++)
      commandHistory[(*startIndex)][i] = lineBuf[i];
    commandHistory[(*startIndex)][i] = '\0';
    (*startIndex)++;
  }
}

/*Display a prompt - the current working directory*/
void getCurWorkDir(char * dirname){
  if (getcwd(dirname, PATH_MAX) == NULL) { /*get current working directory*/
    fprintf(stderr, "Could not obtain current working directory.\n");
    exit(1);
  }
  else {
    printf("Current working directory: %s\n", dirname);
  }
}

stringPair * checkInVars(char * lineBuf, cmdLine * line, stringPair * sPairList){
  if(strcmp("assign",line->arguments[0]) == 0){
    char * name = line->arguments[1];
    char * value = line->arguments[2];
    vAssign(name, value,sPairList);
    /*append(sPairList,tmp);*/
  }
  if(strcmp("unassign",line->arguments[0]) == 0){
    char * name = line->arguments[1];
    vUnassign(sPairList, name);
    printf("TEST1\n");
  }
  if(strcmp("printlist",line->arguments[0]) == 0){
    vEnv(sPairList); 
  }
  return sPairList;
}

stringPair * runCommand(stringPair * sPairList, char ** commandHistory, char * lineBuf, int * startIndex, int * commandsCount){
  if(strcmp("quit",lineBuf)==0)
    exit(0);
  addCommToLog(commandHistory, lineBuf, startIndex, commandsCount);
  if(strcmp("log",lineBuf)==0)
    printComHist(commandHistory,*startIndex,*commandsCount);
  cmdLine * line= parseCmdLines(lineBuf);
  sPairList = checkInVars(lineBuf,line,sPairList);
  changeDir(line);
  execute(line);
  freeCmdLines(line);
  return sPairList;
}

stringPair * checkReuseCommand(stringPair * sPairList, char ** commandHistory,char * lineBuf, int * startIndex, int * commandsCount){
  if(lineBuf[0] == '!'){
    int num = lineBuf[1] - '0';
    if(num > (*commandsCount)){
      printf("command index out of bound, try again..\n");
    }
    else{
      char * reusedLine = commandHistory[num];
      sPairList = runCommand(sPairList, commandHistory, reusedLine, startIndex, commandsCount);
    }
  }
  else{
    sPairList = runCommand(sPairList, commandHistory, lineBuf, startIndex, commandsCount);
  }
  return sPairList;
}



void vUnassign(stringPair * sPairList, char * name){
  int found = 0;  
  stringPair * currSPair = sPairList;
  stringPair * prevSPair = sPairList;
  while(found != 1 || currSPair != NULL){
    if(strncmp(currSPair->name,name,strlen(name)) == 0){/*found it*/
      free(currSPair->name);
      free(currSPair->value);
      prevSPair->next = currSPair->next;
      found = 1;
      free(currSPair);
    }
    prevSPair = currSPair;
    currSPair = currSPair->next;
  }
  if(found == 0)
    printf("Error! cannot remove variable %s since it was not found.\n", name);
  else
    printf("variable %s has been removed successfully.\n", name);
}

/*
 * void vAssign(char * name, char * value, stringPair * sPairList){
 *  int found = 0;
 *  stringPair * headSPair = sPairList ;
 *  stringPair * currSPair = sPairList ;
 *  while(found != 1 && currSPair != NULL){
 *    if(currSPair->name != NULL){
 *      if(strncmp(currSPair->name,name,strlen(name)) == 0){
 *	free(currSPair->value);
 *	currSPair->value = malloc(strlen(value) + 1);
 *	strcpy(currSPair->value,value);
 *	printf("value %s has been overriden successfully.\n", value);
 *	found = 1;
 *	break;
 }
 }
 if(currSPair->name == NULL){
   break;
 }
 if(currSPair->next == NULL)
   break;
 printf("TEST2\n");
 currSPair = currSPair->next;
 }
 if(found == 0){
   printf("TEST3\n");
   currSPair->name = malloc(strlen(name)+1);
   currSPair->value = malloc(strlen(value) + 1);
   printf("TEST4\n");
   strcpy(currSPair->name,name);
   strcpy(currSPair->value,value);
   printf("value %s has been added and assigned to  %s successfully.\n", value, name);
 }
 sPairList =  headSPair;
 
 stringPair * tmpSPair = sPairList;
 while(tmpSPair != NULL){
   printf("< %s : %s >\n",tmpSPair->name,tmpSPair->value);
   tmpSPair = tmpSPair->next;
 }
 }*/

void vAssign(char * name, char * value, stringPair * sPairList){
  stringPair * currSPair = 0;
  if((currSPair = getLink(sPairList,name,value)) == 0){
    printf("link not found\n"); 
  }
  else{
    
  }
}

stringPair * getLink(stringPair * sPairList, char * name, char * value){
  stringPair * currSPair = sPairList ;
  if(currSPair != NULL && currSPair->name == NULL){
    currSPair->name = malloc(strlen(name)+1);
    currSPair->value = malloc(strlen(value) + 1);
  }
  while (currSPair!=NULL && strcmp(name, currSPair->name) != 0){
    currSPair = currSPair->next;
    printf("TEST2\n");
    printf("%p\n",currSPair->name);
    if(strcmp(name, currSPair->name) == 0){
      printf("TEST2\n");
      return currSPair;
    }
  }
  return 0;
}

/*prints all current associations in the environment*/
void vEnv(stringPair * sPairList){
  stringPair * currSPair = sPairList;
  while(currSPair != NULL){
    printf("< %s : %s >\n",currSPair->name,currSPair->value);
    currSPair = currSPair->next;
  }
}


int main(void){
  char dirname[PATH_MAX + 1]; /* To be passed to getcwd system call. */
  char lineBuf[MAX_LINE_SIZE + 1];
  int startIndex = 0 , commandsCount = 0;
  char * commandHistory[MAX_COMMANDS];
  stringPair * sPairList = malloc(sizeof(stringPair));
  while(1){
    getCurWorkDir(dirname);
    if(!gets(lineBuf))
      exit(0);
    if(strlen(lineBuf) > 1){
      sPairList = checkReuseCommand(sPairList, commandHistory, lineBuf, &startIndex, &commandsCount);
    }
    else
      printf("Error! illegal input..");
  }
  return 0;
}

